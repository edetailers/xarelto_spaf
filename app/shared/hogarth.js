var hogarth = {
    metadata: {
        "brandName": "Xarelto",
        "businessUnit": "Cardiovascular",
        "countryCode": "AA",
        "countryName": "Global",
        "indication": "CROSS",
        "languageCode": "AA",
        "languageName": "Global",
        "presentationName": "TEST",
        "YY": "19",
        "WW": "XX",
        "presentationName_PI": ""
    },
    getPresentationName: {
        current: function() {
            var separator = '.';
            return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + hogarth.metadata.presentationName + separator + hogarth.metadata.YY + hogarth.metadata.WW + "-" + hogarth.metadata.languageCode).toUpperCase();
        },
        pi: function() {
            var separator = '.';
            return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + hogarth.metadata.presentationName + "_PI" + separator + hogarth.metadata.YY + hogarth.metadata.WW + "-" + hogarth.metadata.languageCode).toUpperCase();
        },
        references: function() {
            var separator = '.';
            return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + hogarth.metadata.presentationName + "_References" + separator + hogarth.metadata.YY + hogarth.metadata.WW + "-" + hogarth.metadata.languageCode).toUpperCase();
        },
        fromCollection: function(coll) {
            var separator = '.';
            var veeva_name = JSON.parse(app.cache.get('presentation.json')).storyboards[coll].veeva_name;
            return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + veeva_name + separator + hogarth.metadata.YY + hogarth.metadata.WW + "-" + hogarth.metadata.languageCode).toUpperCase();
        },
        root_binder: function(){
            var separator = '.';
            var cfg = JSON.parse(app.cache.get('presentation.json')).veeva_root_binder;
            return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + cfg.name + separator + hogarth.metadata.YY + hogarth.metadata.WW + "-" + hogarth.metadata.languageCode).toUpperCase();
        },
        root_ref_binder: function(){
            var separator = '.';
            var cfg = JSON.parse(app.cache.get('presentation.json')).veeva_root_binder;
            return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + cfg.name+'_REFERENCES' + separator + hogarth.metadata.YY + hogarth.metadata.WW + "-" + hogarth.metadata.languageCode).toUpperCase();
        }
    },
    getSlideName: function(folderName) {
        var separator = '.';
        var folderName = folderName.replace(/[^a-zA-Z0-9]/g, "");
        return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + hogarth.metadata.indication + separator + hogarth.metadata.languageCode + separator + folderName).replace(/\\/g, '_').replace(/\//g, '_').toUpperCase();
    },
    getSlideNameV2: function( folderName ) {
        var coll = arguments[1] || false;
        var separator = '.';
        var folderName = folderName.replace(/[^a-zA-Z0-9]/g, "");
        var cfg = JSON.parse(app.cache.get('presentation.json')).storyboards[coll];
        var indication = (coll && cfg.hasOwnProperty('indication'))? cfg.indication : hogarth.metadata.indication;
        return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + indication + separator + hogarth.metadata.languageCode + separator + folderName).replace(/\\/g, '_').replace(/\//g, '_').toUpperCase();
    },
    getRefName: function(folderName){
        var separator = '.';
        var folderName = folderName.replace(/[^a-zA-Z0-9]/g, "");
        var cfg = JSON.parse(app.cache.get('presentation.json')).veeva_root_binder;

        return (hogarth.metadata.countryCode + separator + hogarth.metadata.brandName + separator + cfg.indication + separator + hogarth.metadata.languageCode + separator + folderName).replace(/\\/g, '_').replace(/\//g, '_').toUpperCase();
    },
    open: {
        references: function(filename) {
            var zipFile = hogarth.getSlideName(filename) + ".zip";
            var presentationToOpen = hogarth.getPresentationName.references();
            console.log('com.veeva.clm.gotoSlide(' + zipFile + ', ' + presentationToOpen + ')');
            com.veeva.clm.gotoSlide(zipFile, presentationToOpen);
        },
        references_v2: function(filename) {
            var zipFile = hogarth.getRefName(filename) + ".zip";
            // var presentationToOpen = hogarth.getPresentationName.references();
            var presentationToOpen = hogarth.getPresentationName.root_ref_binder();
            
            console.log('com.veeva.clm.gotoSlide(' + zipFile + ', ' + presentationToOpen + ')');
            com.veeva.clm.gotoSlide(zipFile, presentationToOpen);
        },
        pi: function() {
            var zipFile = "IT_XARELTO_ITALY PEDVT WIDE_PI_1915-IT_PI.zip";
            var presentationToOpen = hogarth.getPresentationName.pi();
            console.log('com.veeva.clm.gotoSlide(' + zipFile + ', ' + presentationToOpen + ')');
            com.veeva.clm.gotoSlide(zipFile, presentationToOpen);
        }
    }
};