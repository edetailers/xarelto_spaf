# ah-fix-slide-render
-------------------------------------
### Description
##### This module fix multiple present class bind on slides.
-------------------------------------

### How to use:

##### Include module to presentation.json
```
    "ah-fix-slide-render": {
        "id": "ah-fix-slide-render",
        "files": {
            "scripts": [
                "modules/rainmaker_modules/ah-fix-slide-render/ah-fix-slide-render.coffee"
            ]
        }
     }
```    
##### Include module to index.pug
```
    div(data-module='ah-fix-slide-render')
```

##### Dependencies
    You need to use the newer versions of the modules:
    - "ah-correct-events": ">=1.0.0"