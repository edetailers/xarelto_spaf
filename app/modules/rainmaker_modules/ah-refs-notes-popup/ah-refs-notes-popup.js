(function () {
	app.register('ah-refs-notes-popup', function () {
		return {
			publish: {
				spaceCharacter: '&#32;',
				abbr_split: ';' // replace this with split character for abbrs
			},
			excludedCollections: [],
			events: {
				'click .x': 'hide',
				swipeup: 'stopSwipe',
				swipedown: 'stopSwipe',
				swipeleft: 'stopSwipe',
				swiperight: 'stopSwipe'
			},
			states: [{
				id: 'show'
			}],
			setLogoHandler: function () {
				return document.getElementsByClassName('logo')[0].addEventListener('click', (t = this, function (e) {
					return t.invokeRefAndNotesPopup(e);
				}));
				var t;
			},
			invokeRefAndNotesPopup: function (e) {
				if (!this.isSlideMovement && (this.wasUpdatedRefsList || this.wasUpdatedNotesList) && !this.isExcludeCollection()) return this.setCustomStyles(!0), this.getInjectedNotes(), this.initRefAndNotesPopup(null, e), this.show();
			},
			onRender: function (e) {
				var t, s, i;
				return this.setLogoHandler(), this.element = $(e), this.refModule = app.module.get('ah-auto-references-popup'), this.notesModule = app.module.get('notes'), this.loadedNotes = window.ahstrings.notes, this.wasUpdatedRefsList = !1, this.wasUpdatedNotesList = !1, this.injectedNotes = [], app.listenTo(app, 'view-enter', this.autoPopupHandler.bind(this)), app.listenTo(app, 'view-exit', (t = this, function () {
					return t.hide(), t.onSlideUpdate();
				})), app.listenTo(app, 'transition:started', (s = this, function () {
					return s.isSlideMovement = !0;
				})), app.listenTo(app, 'transition:completed', (i = this, function () {
					return i.isSlideMovement = !1;
				})), app.on('state:update', this.autoPopupHandler.bind(this)), app.on('notes:update', this.onNotesUpdate.bind(this)), app.on('references:update', this.onRefsUpdate.bind(this)), this.$refWrapper = this.element.find('.refs-wrapper'), this.$listRefs = this.element.find('.references'), this.notesWrapper = this.element.find('.notes-wrapper'), this.notesData = this.element.find('.notes-data'), this.$listNotes = this.element.find('.footnotes'), this.$listAbbrs = this.element.find('.abbrs'), this.$scroll = this.element.find('.scroll')[0], this.scroll = new IScroll(this.$scroll, {
					scrollbars: 'custom'
				}), this.metadata = Object.keys(window.mediaRepository.metadata());
			},
			onSlideUpdate: function () {
				return this.wasUpdatedRefsList = !1, this.wasUpdatedNotesList = !1;
			},
			onNotesUpdate: function (e) {
				var t;
				if (this.getNotesList(e), (t = this.getNotesList(e)) != null ? t.length : void 0) return this.wasUpdatedNotesList = !0;
			},
			onRefsUpdate: function (e) {
				if (this.getRefsList(e), Object.keys(this.getRefsList(e)).length) return this.wasUpdatedRefsList = !0;
			},
			getNotesList: function (e) {
				return this.notesList = e.list;
			},
			getRefsList: function (e) {
				return this.refsList = e.list;
			},
			refWithReplaceIndex: function (e, t) {
				return this.refModule.replaceIndex(e, t);
			},
			disable: function (e, t) {
				if (t.length <= 0) return e.addClass('disable');
			},
			enable: function (e) {
				return e.removeClass('disable');
			},
			setSingleList: function (e, t) {
				if (t.length === 1) return e.addClass('single');
			},
			removeSingleList: function (e) {
				return e.removeClass('single');
			},
			generateRefsList: function (i) {
				return Object.keys(i).sort((s = this, function (e, t) {
					return s.metadata.indexOf(e) - s.metadata.indexOf(t);
				})).map((n = this, function (e, t) {
					var s;
					return n.filterRefsByTrack(), s = window.mediaRepository.render(e, i[e]), n.refWithReplaceIndex(s, t), s;
				}));
				var n, s;
			},
			filterRefsByTrack: function () {
				var e;
				return e = window.mediaRepository.find('', 'referenceId'), window.mediaRepository.updateAndGetRenderedMetadata(e, 'filterbytrack');
			},
			generateNotesList: function (i) {
				return i.map((n = this, function (e, t) {
					var s;
					return s = t === i.length - 1 ? '' : n.props.spaceCharacter, $('<li data-key=' + e.key + '>' + n.loadedNotes[e.item].description + s + '</li>');
				}));
				var n;
			},
			generateAbbrsList: function (s) {
				return s.map((i = this, function (e, t) {
					return i.createAbbrItem(i.loadedNotes[e], t === s.length - 1, i.loadedNotes[s[t + 1]]);
				}));
				var i;
			},
			createAbbrItem: function (e, t, s) {
				var i;
				return (i = document.createElement('p')).innerHTML = e.description + this.getSeparator(e, t, s), i;
			},
			getSeparator: function (e, t, s) {
				return s && s.isAbbreviation && !s.isSentence ? this.props.abbr_split + this.props.spaceCharacter : e.isSentence ? this.props.spaceCharacter : '.' + this.props.spaceCharacter;
			},
			getFilterAbbrsList: function (e) {
				return e.filter((t = this, function (e) {
					if (t.loadedNotes[e].isAbbreviation) return e;
				}));
				var t;
			},
			getFilterNotesList: function (e) {
				return e.filter((i = this, function (e) {
					if (!i.loadedNotes[e].isAbbreviation) return e;
				})).map((s = this, function (e, t) {
					return {
						item: e,
						key: s.notesModule._generateLetterOrNumber(t + 1)
					};
				})).filter((t = this, function (e) {
					return t.injectedNotes.indexOf(e.item) < 0;
				}));
				var t, s, i;
			},
			getInjectedNotes: function () {
				return this.injectedNotes = this.notesModule.getInjectedNotes(app.slideshow.get());
			},
			autoPopupHandler: function (t) {
				var s;
				return $('#' + t.id).find('[data-reference-id], [data-notes]').off('tap.auto-references-popup').on('tap.auto-references-popup', (s = this, function (e) {
					return s.setCustomStyles(!1), s.injectedNotes.length = 0, s.initRefAndNotesPopup(t, e), s.show();
				}));
			},
			initRefAndNotesPopup: function (e, t) {
				return t.stopPropagation(), this.enable(this.$refWrapper), this.enable(this.notesWrapper), this.removeSingleList(this.$refWrapper), this.removeSingleList(this.notesWrapper), this.$listRefs.empty(), this.$listNotes.empty(), this.$listAbbrs.empty(), this.addRefsNodes(this.refsList), this.addNotesNodes(this.notesList), this.scroll.refresh(), this.scroll.scrollTo(0, 0);
			},
			addRefsNodes: function (e) {
				var rClass = app.slideshow.get() + '_refs';
				this.$refWrapper.get(0).className = 'refs-wrapper ' + rClass;

				return this.disable(this.$refWrapper, Object.keys(e)), this.setSingleList(this.$refWrapper, Object.keys(e)), this.$listRefs.append(this.generateRefsList(e)), this.$refWrapper.append(this.$listRefs);
			},
			addNotesNodes: function (e) {
				var nClass = app.slideshow.get() + '_notes';
				this.notesWrapper.get(0).className = 'notes-wrapper ' + nClass;

				return this.disable(this.notesWrapper, e), this.setSingleList(this.notesWrapper, e), this.$listNotes.append(this.generateNotesList(this.getFilterNotesList(e))), this.$listAbbrs.append(this.generateAbbrsList(this.getFilterAbbrsList(e))), this.notesData.append(this.$listAbbrs), this.notesData.append(this.$listNotes);
			},
			setCustomStyles: function (e) {
				return this.element[e ? 'addClass' : 'removeClass']('custom-refs-notes-popup');
			},
			stopSwipe: function (e) {
				return e.stopPropagation();
			},
			show: function () {
				return this.goTo('show');
			},
			hide: function () {
				return this.reset();
			},
			isExcludeCollection: function () {
				var e;
				return e = app.slideshow.resolve().slideshow, this.excludedCollections.indexOf(e) > -1;
			}
		};
	});
}).call(this);
