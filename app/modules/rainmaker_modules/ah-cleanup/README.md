ah-cleanup
============

Module cleans number of visited slides (number is set in property limit) from stack every time period (time period is set in timeout property)

## How to add to the project:  
  Copy folder `ah-cleanup` to `app/modules/rainmaker_modules` then add this markup `div(data-module="ah-cleanup")` to `index.pug`.
  
Add links to `platforms/rainmaker/presentation.json` to modules object: 
```        
"ah-cleanup": {
  "id": "ah-cleanup",
  "files": {
    "scripts": [
      "modules/rainmaker_modules/ah-cleanup/ah-cleanup.js"
    ]
  }
}
```

## Changelog

### Version 1.0.1
### 2018-03-06 - A.Shapovalova
- slide:enter event replaced with update:anthill-current to fix appearing slide without background, which then appears some time later

### Version 1.0.0
### 2018-02-27 - Nina Koval
- module was updated according to jslint rules

### Version 0.5.0
### 2016-06-22 - Roman Kovalenko
- version of module ag-cleanup, on which ah-cleanup is based
