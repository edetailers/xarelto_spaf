# Localizer Helper

This module assists Anthill Localizer in navigating the detailer to the correct view for a slide.
It works in the same way as Fusion links.json functionality.

Localizer checks if app.localizer_helper has a property with the name of the SlideID it wants to naviaget to.
If the property is a method, it calls it and then takes a screenshot.

If your slide is in a custom popup or depends on ceratin state or settings, you can make them in a method here to allow correct screenshotting.

* To consider your implementation correct:* 
You should be able to open the detailer, call app.localizer_helper.YourSlideID() and arrive at the desired slide with no console errors.


Set in PRESENTATION.JSON
Set props to slide

"type": "inline" | "custom" | "collection" (Default: "popup" setup for all PopupSlide that haven't type)
"mainSlide": "idMainSlide" custom Id from main flow. If empty (hasn't field), will setup default start path slide. (See config)

## Changelog

* add creating current(default) structure