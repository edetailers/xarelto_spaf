ah-veeva-open-media-file
=========

Opens media files and saves response monitoring  to update.

## Usage

#### Include module to presentation.json

```

"ah-veeva-open-media-file": {
    "id": "ah-veeva-open-media-file",
    "files": {
        "scripts": [
            "modules/rainmaker_modules/ah-veeva-open-media-file/ah-veeva-open-media-file.coffee"
        ]
    }
}

 ```   
 

#### 2) Include following tag in Accelerator element in master template:

```#veevaOpenMediaFile(data-module="ah-veeva-open-media-file")```

#### 3) Get module:

```veevaOpenMediaFile = app.module.get 'veevaOpenMediaFile'```

#### 4) Dependencies:
  `ah-veeva-monitoring`: ">=1.1.0"
  `ah-monitoring-tracker'`: ">=1.1.0"
  `ah-veeva-api'`: ">=0.0.1"
  `ah-storage-data'`: ">=1.1.0"