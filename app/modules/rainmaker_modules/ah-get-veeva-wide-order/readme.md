# ah-get-veeva-wide-order

#### Use this module for getting slides ordering in veeva wide presentation.

##### Include module to presentation.json
```
"ah-get-veeva-wide-order":{
    "id": "ah-get-veeva-wide-order",
        "files": {
        "scripts": [
            "modules/rainmaker_modules/ah-get-veeva-wide-order/ah-get-veeva-wide-order.coffee"
        ]
    }
}
```

##### Include module to index.jade/index.pug
```
#veevaWideOrder(data-module="ah-get-veeva-wide-order")
```
