# ah-veeva-monitoring module
-------------------------------------
### Description
##### This module for submit monitoring(standard, custom) for veeva clm.
-------------------------------------

### How to use:
##### 1. Add "veeva-library.js" and "veeva-library-extends.js" to "app/_vendor" folder
##### 2. Include "veeva-library.js" and "veeva-library-extends.js" to "app/platforms/rainmaker/config.json" file
```
"dependencies": [
    { "src": "_vendor/veeva-library.js"}
  ],
 ```
##### 3. Include module to presentation.json
```
 "ah-veeva-monitoring": {
            "id": "ah-veeva-monitoring",
            "files": {
                "scripts": [
                    "modules/rainmaker_modules/ah-veeva-monitoring/ah-veeva-monitoring.coffee"
                ]
            }
        }
 ```
##### 4. Include module to index.jade/index.pug
```
#veevaMonitoring(data-module='ah-veeva-monitoring')
```
##### Configs

MonitoringItem config:
For submit total time on slide you can set 'isTotalTicks' = true
For logging submit data you can set 'isDebugMode' = true
For changing minimal visit time duration for view you can set time in 'MIN_VISIT_TIME_MS'. For example
if slide visit time < MIN_VISIT_TIME_MS - slide will be not submitted
```
    config: {
        isTotalTicks: false,
        isDebugMode: true
      }
    MIN_VISIT_TIME_MS: 1000
```


VeevaMonitoring config:
For submit analytics from 'analytics.json' file you can use config :
```
    config: {
        isDataFromFile: true
      }
    DATA_FILE: 'analytics.json'
```

##### Dependencies
    Libraries: _vendor/veeva-library.js
    Modules:
        - ah-history v2.0.0
        - ah-monitoring-tracker v.1.0.0

##### Standard monitoring:
Module creates and submit data automatically, according to view events (view-enter, view-exit)

##### Custom monitoring:
For submit custom or pdf data, you can use function "submitCustom" or "submitPdf" of "ah-monitoring-tracker" module (fields 'id', 'type' and 'description' is required!!! For 'custom' data 'description' field should be unique! ):
```
  data = {
        id: id,
        type: "custom" or "pdf",
        description: "description",
        answer: "answer"
      }
  app.module.get('monitoringTracker').submitCustom(data)
  app.module.get('monitoringTracker').submitPdf(data)
```
Note, if you want to monitor CLM Insight, you can change data.type to "insight" in example below
## Changelog
### version 1.0.1
    fix linter issues
    add types for inlineslideshow and pdf
### version 1.1.0
    add possibility to submit monitoring from file
    update monitoring types - move to separate config class
    add MIN_VISIT_TIME_MS - for excluding monitoring of slides, where time < MIN_VISIT_TIME_MS. Please set larger then 0 (for example, 1000) for correct working with ah-veeva-open-media-file module.
    update presentationExit functional - add veeva wide check
    refactor functions called on input events
    update custom monitoring functional (change input data values)
### version 1.1.1
	fix issue with using Object.values() for ios 10.2
### version 1.1.2
	fix callback error with wrong monitoring data
### version 1.1.3
	fix issue with send duplicate data

