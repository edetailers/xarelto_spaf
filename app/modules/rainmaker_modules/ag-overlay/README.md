ag-overlay
============

Module creates an overlay to the presentation.

## Usage

1) Include following tag in master template (index.pug):

```#overlayPopupCustom.custom-popup(data-module='ag-overlay')```

## Settings

The following properties can be set on the above element:

- noBackground BOOLEAN [true] will disable overlay background.
- noCloseBtn BOOLEAN [true] will hide close button.
- content STRING set placeholder on missing html content.
- delay INTEGER delay on unlock presentation.
- type STRING set type of opened content.

### Example

```#overlayPopupCustom.custom-popup(data-module='ag-overlay', noBackground=false, noCloseBtn=false, content='No content available', delay=0, type='popup')```

## Changelog

### Version 0.6.0 2018-03-21
- Fixed wrong behaviour on exit state when unlock presentation.

### Version 0.6.1 2018-04-17
- Add functionality for set 'state-default' for popup (The same as slide). Variable for this class is defaultClassSlide and set this class if slide hasn't active state.
