# ah-check-lock
-------------------------------------
##### This module fix unlock swipe when open popup and make some manipulation with toolbar

### DEPENDENCIES:
```
    footer#toolbar(data-module='ah-toolbar', minimized='')
    #viewer.custom-overlay(data-module="ag-viewer")
    #overlay-popup-custom.custom-popup(data-module='ag-overlay')
    #smpc-overlay.custom-popup(data-module='ag-overlay') - optional 
```

### HOW TO USE:

##### Include module to presentation.json
```
"ah-check-lock": {
   "id": "ah-check-lock",
   "files": {
       "scripts": [
           "modules/rainmaker_modules/ah-check-lock/ah-check-lock.coffee"
       ],
       "styles": []
   }
} 
```
##### Include module to index.jade/index.pug or other place, like menu wrapper
```
 div(data-module='ah-check-lock')
```
 


