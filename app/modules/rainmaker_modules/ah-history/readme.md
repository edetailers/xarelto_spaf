# ah-history module
-------------------------------------
### Description
##### This module the triggered event when current view (slide or popup) updated.
##### In callback you receive data about view.
-------------------------------------

### How to use:

##### Include module to presentation.json
```
"ah-history": {
            "id": "ah-history",
            "files": {
                "scripts": [
                    "modules/rainmaker_modules/ah-history/ah-history.coffee"
                ]
            }
 }
 ```    
##### Include module to index.jade/index.pug
```
#ahHistory(data-module='ah-history')
```

##### Dependencies
    You need to use the newer versions of the modules:
    - "ah-correct-events": ">=1.0.0"
    - "ah-inline-slideshow": ">=1.0.0"
    - "ag-overlay": ">0.5.0"

##### Example of using - use event 'view-enter' or 'view-exit' to monitor changes current view:

```
app.listenTo app, 'view-enter', (data)=> do something, data - info about current view
```
