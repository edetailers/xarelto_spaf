# ah-veeva-library-extends
-------------------------------------
### Description
##### Module for monitoring last slide/popup in veeva in case presentation exit
-------------------------------------

### How to use:

##### Include module to presentation.json
```
"ah-veeva-library-extends": {
            "id": "ah-veeva-library-extends",
            "files": {
                "scripts": [
                    "modules/rainmaker_modules/ah-veeva-library-extends/ah-veeva-library-extends.coffee"
                ]
            }
        },
 ```
##### Include module to index.jade/index.pug
```
#veevaExtends(data-module='ah-veeva-library-extends')
```

##### Dependencies
    Libraries: _vendor/veeva-library.js
    Modules:
        - ah-history v2.0.0
        - ah-monitoring-tracker v1.0.0
        - ah-veeva-monitoring v.1.1.0

##### Monitoring last slide/popup:
Module extend veeva library native function "com.veeva.clm.createRecordsOnExit",
which automatically called in veeva clm on presentation exit
