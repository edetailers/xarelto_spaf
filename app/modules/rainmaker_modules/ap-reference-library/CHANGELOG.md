### Version 1.0.5 2018-10-10
### Andrii Romanyshyn
- Remove "usereferenceslistnumeration" property functionality

### Version 1.0.4 2018-09-27
### Dryzhuk Andrii
- change ag to window.ag in monitor usage function

### Version 1.0.3 2018-07-16
### Oleksandr Melnyk
- fix css linter warnings

### Version 1.0.2 2018-06-04
### Oleksandr Tkachov
- Update code to use current ap-media-repository module functionality, add "usereferenceslistnumeration" property for rendering references numeration according to their

### Verson 1.0.1 2018-05-31
### Oleksandr Tkachov
- Update code to use current ap-media-repository module functionality.
