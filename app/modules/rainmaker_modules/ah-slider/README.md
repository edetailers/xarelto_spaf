ah-slider
=========

Slider for state change on slide.

## Usage

#### 1) Include module to presentation.json 

```

"ah-slider": {
    "id": "ah-slider",
    "files": {
        "scripts": [
            "modules/rainmaker_modules/ah-slider/ah-slider.coffee"
        ],
        "styles": [
            "modules/rainmaker_modules/ah-slider/ah-slider.styl"
        ]
    }
},

 ```   
#### 2) Include next template to our slide pug:

##### Template:
```
    mixin slider(options, customClass)
        - var sliderClass = customClass ? customClass: 'common-slider'
        .slider-wrapper(class = sliderClass data-module='ah-slider' data-stop-swipe)&attributes(options)
            .ui-slider-handle
```
##### Use on slide:
```
    - var sliderProps = {
        'min': 0,
       ' max': 100,
        'value': 0,
        'step': 1,
        'updateSlideState': true,
        'steps': [
            {
                stateId: 'state-1',
                stopPoint: 20
            },
            {
                stateId: 'state-2',
                stopPoint: 30
            },
            {
                stateId: 'state-3',
                stopPoint: 50
            },
            {
                stateId: 'state-4',
                stopPoint: 80
            },
            {
                stateId: 'state-5',
                stopPoint: 100
            }
        ]
    }
    
    +slider(sliderProps, 'example-class')
```

#### 3) Set states in slide coffee script

```
    @states = [
      {id: 'state-1'},
      {id: 'state-2'},
      {id: 'state-3'},
      {id: 'state-4'},
      {id: 'state-5'}
    ]
```

### Author
Andrii Romanyshyn

## Changelog

```
*   v.1.0.0
        Created ah-slider module
```