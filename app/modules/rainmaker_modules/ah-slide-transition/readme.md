# ah-slide-transition
-------------------------------------
Version: 0.1.0
This module triggers events in the started movement and in the completion of the slide movement

###How to use:
  Copy folder ah-slide-transition to `app/modules/rainmaker_modules` then add this markup `div(data-module="ah-slide-transition")` to `index.pug`

  Add links for module into `platforms/rainmaker/presentation.json` to modules object like this:
  ```
    "ah-slide-transition": {
        "id": "ah-slide-transition",
        "ignoreVeeva": true,
        "files": {
            "scripts": [
                "modules/rainmaker_modules/ah-slide-transition/ah-slide-transition.coffee"
            ]
        }
    }
  ```

  Add to `app/platforms/rainmaker/config.json`
  ```
    "constTransitionSpeed": {
      "slow": 1100,
      "default": 800,
      "fast": 300
    },
  ```
Dependencies:
 - "ah-correct-events": ">=1.0.0"

###Example of using - use event `transition:started` or `transition:completed`

```
app.listenTo app, 'transition:started', (data)=>
```