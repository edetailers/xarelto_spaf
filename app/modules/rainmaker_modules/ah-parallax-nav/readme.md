# ah-parallax-nav
-------------------------------------

This modules for slide parallax animation

###How to use:
  Copy folder ah-parallax-nav to `app/modules/rainmaker_modules` then add this markup `div(data-module="ah-parallax-nav")` to `index.pug`

  Add links for module into `platforms/rainmaker/presentation.json` to modules object like this:
  ```
    "ah-parallax-nav": {
        "id": "ah-parallax-nav",
        "ignoreVeeva": true,
        "files": {
            "scripts": [
                "modules/rainmaker_modules/ah-parallax-nav/request-animation-frame.coffee",
                "modules/rainmaker_modules/ah-parallax-nav/slide-animation.coffee",
                "modules/rainmaker_modules/ah-parallax-nav/ah-parallax-nav.coffee"
            ],
            "styles": [
                "modules/rainmaker_modules/ah-parallax-nav/ah-parallax-nav.styl"
            ]
        }
    }
  ```
Dependencies:
  `ah-correct-events`: ">=1.1.0"
  `ah-slide-transition` : ">=0.1.0"
  `ah-dynamic-tracks` : ">=1.0.0"
###Options

| Nme                 | Units   | Type    | Default | Description                                                          |
|:--------------------|:-------:|:-------:|:-------:|:---------------------------------------------------------------------|
| duration            | ms      | number  | 800     |Animation duration. Should coincide with the duration of the engine.  |
| offsetVertical      | percent | number  | 135     |Slide offset. It means the distance between the slides by vertical    |
| OffsetHorizontally  | percent | number  | 130     |Slide offset. It means the distance between the slides by horizontally|
| isUseCssTransition  |         | boolean | true    |Use CSS transition or requestAnimationFrame                           |

###API:
- `enable()`
    Description: Use this method to enable module
- `disable()`
    Description: Use this method to disable module
- `setUltraMode()`
    Description: Use this method to OFF animation
- `setDefaultMode()`
    Description: Use this method to ON animation


Change Log
======================

### Version 0.1.3
### 2018-07-13 - Anastasiya Shapovalova
-  Fix issue with skipping animation on slides right after going to new collection

### Version 0.1.2
### 2018-06-22 - Serhii Synianskyi
-  Change script according to dynamic structure

### Version 0.1.1
### 2018-05-25 - Serhii Synianskyi
-  Add access to class methods

### Version 0.1.0
### February 2018 - Serhii Zviahelskyi, Serhii Synianskyi
-  Initial commit