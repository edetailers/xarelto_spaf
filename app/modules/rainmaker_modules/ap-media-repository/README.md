# ap-media-repository

##### This module provides a database interface for accessing meta information of media content.

### How to use:

##### Include module to presentation.json
```

"ap-media-repository": {
    "id": "ap-media-repository",
     "files": {
        "templates": [],
        "scripts": [
            "modules/ap-media-repository/ap-media-repository.js"
        ],
        "styles": [
            "modules/ap-media-repository/renderers.css"
        ]
    }
}
 ```
##### Include module to index.jade/index.pug/index.html
```
    <div data-module="ap-media-repository" usereferenceslistnumeration></div>
```  
    - usereferenceslistnumeration: If set usereferenceslistnumeration prop references numeration will be rendered according to the reference position in the list instead of using "referenceId" property.

##### API
Module methods are available in its instance through window.mediaRepository global variable.

    - metadata() - returns the meta data object representing the meta database at runtime.
    - find(searchTerms, attributesToBeSearched) - searches through the meta data object for given search terms in given attributes.
    - setMetada(attribute, key, content) - set new content for an attribute of the metadata object.
    - getMetada(attribute) - get attribute content.
    - addRenderer(renderer) - adds a {renderer} to the {renderer} chain.
    - render(file, meta, options) - renders a media entry. Uses the first matching renderer in the renderer chain.
    - updateAndGetRenderedMetadata(data, filtrationOptions) - returns filtered with filtrationOptions meta data object which will be used for rendering. filtrationOptions is a string of options separated with comma. Each option value should have a filtration handler function set in the module config (as an "option": "handlerFunctionName" key-value pair). For example
    ```
        var config = {
            metadataFiltrationHandlers: {
                'filterbytrack': 'filterTrack'
            }
        };
    - enableReferencesListNumeration - enable references numeration rendering according to their position in the rendered list instead of using "referenceId" property.
    - disableReferencesListNumeration - disable the above described functionality.
    ```
