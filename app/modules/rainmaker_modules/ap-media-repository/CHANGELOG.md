### Version 1.0.5 2018-10-10
### Andrii Romanyshyn 
- Added usereferenceslistnumeration property. If set to usereferenceslistnumeration property references numeration will be rendered according to the reference position in the list instead of using "referenceId" property.

### Version 1.0.3 2018-07-16
### Oleksandr Melnyk
- fix js linter errors.

### Version 1.0.2 2018-06-04
### Oleksandr Tkachov
- add functionality for rendering references numeration according to their position in the list instead of using "referenceId" property.

### Version 1.0.1 2018-05-31
### Oleksandr Tkachov
- add functionality for filtering metadata by current active presentation flow, getting list of rendered metadata filtered with filtration handlers.
