# ah-pie module

This module uses the d3(v4) library:

* [Source](https://github.com/d3/d3/releases/download/v4.7.4/d3.zip)
* ```npm install d3```
* ```bower install d3```

### v 0.1.0
 *(updated by: Anton Havryliaka, reviewed by: Roman Savitskyi)*
 - Added animation for pie
 - Use property `animationDuration` to set total duration of transition. Pie will now automatically calculate transition for every `path` and animate smoothly

#### Include module to presentation.json
```
"ah-pie": {
    "id": "ah-pie",
    "files": {
        "scripts": [
            "modules/rainmaker_modules/ah-pie/ah-pie.coffee"
        ],
        "styles": [
            "modules/rainmaker_modules/ah-pie/ah-pie.styl"
        ]
    }
}
```

#### Config (default):
```
{
    outerRadius: 100
    innerRadius: 0
    padAngle: 0.02
    viewModeLabels: 'inner' # inner || outer || ''
    labelsRadius: false
    margins: [0, 0, 0, 0]
}
```

### Example:
```
#ahPie(
    data-module="ah-pie"
    pad-angle='0'
    inner-radius="35"
    outer-radius="120"
    view-mode-label="inner"
    labels-radius="150"
    margins="[50, 150, 50, 150]"
)
```

## API

```
app.module.get('ahPie').setData([
  {title: "First",  value: 1},
  {title: "Second", value: 3},
  {title: "Third",  value: 5},
  {title: "Fours",  value: 2},
  {title: "Fifth",  value: 8},
  ...
])
```


![](./readme.jpg)
