/**
 * Implements Bottom Toolbar.
 * --------------------------
 * @module ap-toolbar.js
 * @requires jquery.js, all other classes referenced in toolbar.html
 * @author Alexander Kals, David Buezas, Andreas Tietz, antwerpes ag
 */
app.register('ap-toolbar', function () {
	/**
	 * Implements Bottom Toolbar.
	 * --------------------------
	 * To add buttons to the toolbar add the following to toolbar.html:
	 *
	 *        <div class='button' data-toolbar-state='...' data-module-to-load='...'></div>
	 *
	 * where:
	 *        - data-toolbar-state = ['minimized' | 'maximized' | 'hidden']
	 *            states the end state of the toolbar when the button is tapped
	 *        - data-module-to-load = A class name (must inherit from Module)
	 *            states the class from which an instance will be created and loaded inside the content container
	 *
	 * Also add the module to be loaded within the 'content'-container
	 *  e.g.:
	 *    <div data-module='ap-overview' hide></div>
	 *
	 * If you don't need a module, just remove the button and the entry
	 * @class ap-toolbar
	 */
	var self;
	return {
		publish: {
			hide: false,
			microsite: false,
			excludedSlides: 'ClusterSlide',
			overviewPopup: 'OverviewPopupSlide',
			collectionPIPopups: {
				'c1_2019_spaf_cardio_collection': 'SpafCardioPiPopupSlide',
				'c1_2019_pedvt_collection': 'PEDVTPiPopupSlide',
				'c1_2019_tom_pedvt_sda_collection': 'TomPEDVTSDAPrescribingInformationPopupSlide',
				'c1_2019_tom_pedvt_ti_collection': 'TomPEDVTTIPrescribingInformationPopupSlide',
				'c1_2019_tom_pedvt_ext_collection': 'TomPEDVTExtPrescribingInformationPopupSlide',
				'c1_2019_tom_pedvt_pe_collection': 'TomPEDVTPEPIPopupSlide',
				'c1_2019_pe_dvt_vte_spaf_combo_collection': 'PEDVTVTESPAFComboPrescribingInformationPopupSlide',
				'c1_2019_vp_pad_collection': 'VPPADPIPopupSlide',
				'c1_2019_vp_cad_collection': 'VPCADPrescribingInformationPopupSlide',
				'c2_2019_spaf_cad_dual_collection': 'SPAFComboPrescribingInformationPopupSlide',
				'c1_2019_pedvt_combo_collection': 'PEDVTComboPrescribingInformationPopupSlide',
				'c2_2019_start_collection': 'CorePrescribingInformationPopupSlide',
				'c2_2019_cad_diabetes_collection': 'CADDiabetesPiPopupSlide',
				'c2_2019_cad_reinfarction_collection': 'CADReInfarctionPiPopupSlide',
				'c2_2019_cad_and_pad_collection': 'VPCADPADPiPopupSlide',
				'c2_2019_vp_pad_hor_collection': 'VPPADPHORPiPopupSlide',
				'c2_2019_vp_pad_wic_collection': 'VPPADWICPiPopupSlide',
				'c2_2019_vp_pad_cad_collection': 'VPPADCADPiPopupSlide',
				'c2_2019_pe_dvt_pad_sdr_collection': 'PEDVTPADSDRPIPopupSlide',
				'c2_2019_pe_dvt_pad_wic_collection': 'PEDVTPADWICPIPopupSlide'
			},
			delay: 100
		},
		excludedPopups: ['OverviewPopupSlide', 'SpafCardioPiPopupPopupSlide', 'PiPopupPopupSlide', 'TomPEDVTTIPrescribingInformationPopupSlide', 'TomPEDVTSDAPrescribingInformationPopupSlide', 'TomPEDVTExtPrescribingInformationPopupSlide', 'TomPEDVTPEPIPopupSlide', 'PEDVTPiPopupSlide', 'VPPADPIPopupSlide', 'SPAFComboPrescribingInformationPopupSlide', 'PEDVTComboPrescribingInformationPopupSlide', 'VPCADPrescribingInformationPopupSlide', 'CADDiabetesPiPopupSlide', 'VPCADPADPiPopupSlide', 'CADReInfarctionPiPopupSlide', 'VPPADCADPiPopupSlide', 'VPPADPHORPiPopupSlide', 'PEDVTPADSDRPIPopupSlide', 'PEDVTPADWICPIPopupSlide'],
		events: {
			'tap': 'handleEvent',
			'tap .button[data-module-to-load=\'ap-overview\']': function (event) {
				var overlay = app.view.get('overlayPopupCustom');
				overlay.close();
				app.view.get('ahRefsNotesPopup').reset();
				setTimeout(function () {
					overlay.load(this.props.overviewPopup);
					app.once('closed:overlay', function (element) {
						element.classList.remove('active');
					}.bind(this, event.target));
				}.bind(this), overlay.props.delay + self.props.delay);
				self.handleEvent(event);
				app.$.toolbar.hide();
			},
			'tap .button[data-module-to-load=\'ap-specific-product-characteristics\']': function (event) {
				// var overlay = app.view.get('overlayPopupCustom');
				// overlay.close();
				// app.view.get('ahRefsNotesPopup').reset();
				// setTimeout(function () {
				// 	overlay.load(this.props.collectionPIPopups[app.slideshow.resolve().slideshow]);
				// 	app.once('closed:overlay', function (element) {
				// 		element.classList.remove('active');
				// 	}.bind(this, event.target));
				// }.bind(this), overlay.props.delay + self.props.delay);

				app.$.specificProductCharacteristics.openPdf();

				self.handleEvent(event);
				app.$.toolbar.hide();
			},
			'tap .button[data-module-to-load=\'ap-reference-library\']': function () {
				this.closeOverlay();
			},
			'tap .button[data-module-to-load=\'ap-follow-up-mail\']': function () {
				this.closeOverlay();
			},
			'tap .bar .button.notepad': function () {
				app.$.notepad.toggleNotepad();
			},
			'tap .bar .button.jumpToLastSlide': function () {
				self.jumpToLastSlide();
				app.$.menu.updateCurrent();
				app.$.toolbar.hide();
			},
			'swipeleft': function (event) {
				event.stopPropagation();
			},
			'swiperight': function (event) {
				event.stopPropagation();
			},
			'swipeup': function (event) {
				event.stopPropagation();
			},
			'swipedown': function (event) {
				event.stopPropagation();
			}
		},
		states: [
			{
				id: 'minimized',
				onEnter: function () {
					// app.util.transformElement(this.$el, '-webkit-translate3d(0,100%,0)');
				}
			},
			{
				id: 'hidden',
				onEnter: function () {
					// app.util.transformElement(this.$el, '-webkit-translate3d(0,667px,0)');
				}
			},
			{
				id: 'maximized',
				onEnter: function () {
					// app.util.transformElement(this.$el, '-webkit-translate3d(0,0,0)');
				}
			}
		],
		onRender: function () {
			self = this;
			app.$.toolbar = this;
			this.activate();
			if (this.props.hide) {
				this.hide();
			}
			this.$el = document.getElementsByClassName('ap-toolbar')[0];
			app.listenTo(app.slideshow, 'load', this.activate);
			$('.ap-toolbar').attr('data-state', 'hidden');
			this.goTo('hidden');
		},
		activate: function () {
			if (self.props.excludedSlides.indexOf(app.slideshow.get()) === -1) {
				self.enable();
			} else {
				self.disable();
			}
		},
		isActive: function (slideId) {
			return this.excludedPopups.indexOf(slideId) !== -1;
		},
		closeOverlay: function () {
			var overlay = app.view.get('overlayPopupCustom');
			if (overlay.slideId && this.isActive(overlay.slideId)) {
				overlay.close();
			}
		},
		onRemove: function () {},
		onEnter: function () {},
		onExit: function () {},
		setMicrosite: function () {
			self.microsite = true;
			$(this.$el).addClass('microsite');
		},
		hide: function () {
			app.$.trigger('toolbar:hidden');
			var $joystick = $('.joystick');
			$('.ap-toolbar').attr('data-state', 'hidden');
			$joystick.fadeIn();
			this.goTo('hidden');
		},
		disable: function () {
			self.$el.classList.add('hidden');
		},
		enable: function () {
			self.$el.classList.remove('hidden');
		},
		open: function () {},
		jumpToLastSlide: function () {
			var collectionLength = app.slideshow.getLength();
			var lastSlide = app.model.getStoryboard(app.slideshow.getId()).content[collectionLength - 2];
			app.$.BackNavigation.setPrevCollection(app.model.getStoryboard(app.slideshow.getId()).id);
			app.slideshow.goTo(lastSlide);
		},
		handleEvent: function (e) {
			if ($(e.target).hasClass('active')) return;
			var $allButtons = $('.button[data-module-to-load]');
			var $joystick = $('.joystick');
			var $overlay = app.view.get('overlayPopupCustom');
			var target = e.target;
			if ($(target).hasClass(this.props.dataModule)) {
				var state = $(target).attr('data-state');
				var map = {
					hidden: 'minimized',
					maximized: 'hidden',
					minimized: 'hidden'
				};
				self.goTo(map[state]);
				if (!this.isActive($overlay.slideId)) {
					$allButtons.removeClass('active');
				}
				$('input').blur();
				$joystick.fadeIn();
				$(target).attr('data-state', map[state]);
				app.$.menu.hide();
				app.$.trigger('toolbar:hidden');
			}
			var moduleToLoad = $(target).attr('data-module-to-load');
			var currentModule = '';
			if (moduleToLoad) {
				var toolbarState = $(target).attr('data-toolbar-state');
				setTimeout(function () {
					// avoid same touch event triggering input elements focus
				}, 0);
				if (toolbarState) {
					app.$.toolbar.goTo(toolbarState);
					app.$.menu.hide();
				}
				if (currentModule === moduleToLoad) return;
				app.$.trigger('open:' + moduleToLoad);
				$joystick.fadeOut();
				$allButtons.not($(target)).removeClass('active');
				$allButtons.each(function (index, item) {
					var otherModule = $(item).attr('data-module-to-load');
					if (moduleToLoad !== otherModule) {
						app.$.trigger('close:' + otherModule);
					}
				});
				$(target).addClass('active');
			}
		}
	};
});
