(function () {
	app.register('ah-auto-references-popup', function () {
		var d, f;
		return d = ',', '-', '–', f = [], {
			publish: {
				isglobalnumeric: !'',
				isfiltertrack: !0,
				// use 8212 for mdash, 8211 for ndash, simple char for changing
				separator: String.fromCharCode(8212)
			},
			events: {},
			states: [],
			getCurrentSlide: function () {
				var e;
				return (e = app.slide.get(app.slideshow.get())) != null ? e.el : void 0;
			},
			onRender: function (e) {
				var t, r;
				return this.isGlobalNumeric = this.props.isglobalnumeric, this.isFilterTrack = this.props.isfiltertrack, this.data = window.mediaRepository.metadata(), this.getCurrentSlide() && this.renderReferences(this.getCurrentSlide()), app.on('close:overlay', (t = this, function () {
					return t.autoReferences(t.getCurrentSlide());
				})), app.listenTo(app, 'view-enter', this.autoReferences.bind(this)), app.listenTo(app.slide, 'slide:anthill-render', this.renderReferences.bind(this)), app.listenTo(app.slideshow, 'update:current', (r = this, function () {
					var e;
					if (e = r.getCurrentSlide()) return r.parseReferences(e);
				}));
			},
			onRemove: function (e) {},
			onEnter: function (e) {},
			onExit: function (e) {},
			removePopup: function () {
				'';
			},
			isReferenceId: function (e, t) {
				return !(!e[t] || !e[t].hasOwnProperty('referenceId')) && e[t];
			},
			getIndex: function (e) {
				return this.isGlobalNumeric ? this.getGlobalIndex(e) : this.getLocalIndex(e);
			},
			getLocalIndex: function (e) {
				var t, r, n, i, s;
				for (n in t = [], s = [], this.refList) {
					if (this.refList[n].indexOf('-') > -1) i = this.refList[n].split('-'), s.push(i[0]), s.push(i[1]);
					else if (this.refList[n].indexOf(d) > -1) {for (r in i = this.refList[n].split(d)) s.push(i[r]);} else s.push(this.refList[n]);
				}
				for (n in s) t.indexOf(s[n]) === -1 && t.push(s[n]);
				return n = t.indexOf(e) + 1;
			},
			getGlobalIndex: function (e) {
				var t, r, n, i, s, o;
				for (s in r = {}, t = window.mediaRepository.metadata()) this.isReferenceId(t, s) && (r[s] = this.isReferenceId(t, s));
				for (s in n = mediaRepository.filterTrack(r)) {for (i in n[s]) i === 'referenceId' && n[s][i] === e && (o = Object.keys(n).indexOf(s) + 1);}
				return o;
			},
			getRefByIndex: function (e) {
				var t, r, n, i;
				for (n in r = 1, t = mediaRepository.filterTrack(window.mediaRepository.metadata())) r === e && (i = t[n].referenceId), r++;
				return i;
			},
			getReferencesIds: function (e) {
				var t, r, n, i;
				for (this.refList = [], this.elements = e.querySelectorAll('[data-reference-id]'), r = 0, n = (i = this.elements).length; r < n; r++) t = i[r], this.refList.push(t.getAttribute('data-reference-id'));
				return this.refList;
			},
			parseReferences: function (e, t) {
				var r, n, i, s, o;
				for (this.referenceIds = [], t || (r = app.dom.get(e.id), t = this.getReferencesIds(r)), i = s = 0, o = t.length; s < o; i = ++s) n = t[i], this.referenceMap(n, i);
				return f = Object.keys(this.referenceIds);
			},
			referenceMap: function (e, t) {
				var a, c;
				return a = [], e.split(d).forEach((c = this, function (e) {
					var t, r, n, i, s, o, f;
					if (s = (o = e.split('-'))[0], o.length > 1) {
						for (n = [], t = c.getIndex(s), f = c.getIndex(o[1]), r = t; r <= f;) c.referenceIds[c.getRefByIndex(r)] = !0, r++;
						n.push(t, f), i = n.join( c.props.separator );
					} else i = c.getIndex(s), c.referenceIds[s] = !0;
					return a.length === 0 ? a.push(i) : a = [a, i].join(d);
				})), this.render(a, t), this.referenceIds;
			},
			render: function (e, t) {
				var r;
				return this.isGlobalNumeric || e.indexOf(d) > -1 && (r = e.split(d)).length > 2 && (e = r[0] + '-' + r[r.length - 1]), this.elements[t].textContent = e;
			},
			getResources: function () {
				var n;
				return n = {}, this.isGlobalNumeric ? $.each(window.mediaRepository.metadata(), function (e, t) {
					if (f.indexOf('' + t.referenceId) > -1) return n[e] = t;
				}) : $.each(f, function (r) {
					return $.each(window.mediaRepository.metadata(), function (e, t) {
						if (t.referenceId === f[r]) return n[e] = t;
					});
				}), n;
			},
			autoReferences: function (e) {
				return this.parseReferences(e), app.trigger('references:update', {
					list: this.getResources(),
					slideId: e.id
				});
			},
			renderReferences: function (e) {
				return this.parseReferences(e), app.trigger('references:render', {
					list: this.getResources(),
					slideId: e.id
				});
			},
			replaceLocalIndex: function (e) {
				return '[' + e + ']';
			},
			replaceIndex: function (e, t) {
				var r;
				if (!this.isGlobalNumeric) return ((r = e[0].getElementsByClassName('referenceId')) != null ? r[0] : void 0).innerHTML = this.replaceLocalIndex(++t);
			}
		};
	});
}).call(this);
