# ah-correct-events'
-------------------------------------
##### @module ah-correct-events.coffee
-------------------------------------
Trigger ONLY correct events

This modules trigger new events, like 
    'slide:anthill-render' - correct event for slide render 
    'update:anthill-current' - correct event for changing slideshow
    'update:current-prepared' - This event triggers when user swipe to missing slide (waits when the slide will inserted in the DOM).

If you need you can trigger new necessary events or some custom events. For this you should add method for correct validation new events.  

How to use:  
  Copy folder ah-lock-swipe to app/modules/rainmaker_modules then add this markup div(data-module="ah-correct-events") to index.jade
  
  Add links for module into platforms/rainmaker/presentation.json to modules object like this: 
      "ah-correct-events": {
          "id": "ah-correct-events",
          "files": {
              "scripts": [
                  "modules/rainmaker_modules/ah-correct-events/ah-correct-events.coffee"
              ],
              "styles": []
          }
      }
      
   Please use on your slides or modules new events where it necessary.
      
### Author SuperUser


