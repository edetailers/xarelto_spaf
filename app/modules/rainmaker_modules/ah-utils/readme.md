# XHR methods for get data
# Make GET request to path
# * How use: #ahUtils(data-module='ah-utils')
# * Example:
```
app.module.get("ah-utils").getSlideData( SLIDE_ID, ( data )=>
      console.log( data )
    )
```
Or other method
```
app.module.get("ah-utils").getData( path, ( data )=>
      console.log( data )
    )
```
# version 1.1.0 - add method stripTags for remove tags from texts