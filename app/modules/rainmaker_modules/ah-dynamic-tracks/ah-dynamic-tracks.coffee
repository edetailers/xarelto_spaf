class AhDynamicTracks
  constructor: (options)->
    @publish =
      STORAGE: 'ahStorageData'
      LOCKER: 'ahLocker'
      HISTORY: 'ahHistory'
    
    @slideshows = options.slideshows or {}
    @structures = options.structures or {}
  
  init: ->
    @customTracks = {}
    @_handleSwipe = @handleSwipe.bind(@)
    app.slideshow.on 'load', @handleLoad.bind(@)
    app.listenToOnce app.slide, 'slide:anthill-render', @handleLoad.bind(@) if !app.isVeevaWide
    @handleLoadSlide() if app.isVeevaWide
    app.slideshow.on 'update:anthill-current', @setCurrentCustomTrack.bind(@)
  
  isMovingForward: ->
    @onCustomPath() and @forward
  
  setCurrentCustomTrack: (data)->
    if not @onCustomPath() and @currentCollectionModel
      @setTrack {track: @getCurrentCustomTrack(data)}
  
  onCustomPath: -> @getCustomTrack() and @isOnCustomTrackPath()
  
  getCurrentCustomTrack: (data)->
    tracks = @getCustomTracks()
    Object.keys(tracks).find (track)=>
      @isPresent tracks[track], data.current.id
  
  isActiveCustomNav: ->
    @currentCollectionModel and @isOnCustomTrackPath()
  
  handleSwipe: (event)->
    return unless @isActiveBasisCollection()
    switch event.type
      when 'swipeleft' then @goToNext()
      when 'swiperight' then @goToPrev()
  
  goToNext: ()->
    @forward = true
    if @isActiveCustomNav() then @navigateOnCustomTrack(true) else app.slideshow.next()
  
  goToPrev: ()->
    @forward = false
    if @isActiveCustomNav() then @navigateOnCustomTrack(false) else app.slideshow.prev()
  
  isActiveBasisCollection: ->
    @history.stack.length is 1
  
  handleLoad: ->
    @handleLoadSlide()
    @clearTrack()
  
  handleLoadSlide: ->
    @currentCollection = @getCollection()
    @currentCollectionModel = @getCollectionModel()
    @initSwipe()
  
  lock: ->
    app.isDynamicTrack = true
    @locker.nativeLock()
    @customSwipe 'add'
  
  unlock: ->
    app.isDynamicTrack = false
    @locker.nativeUnlock()
    @customSwipe 'remove'
    @forward = ''
  
  initSwipe: ->
    if @currentCollectionModel then @lock() else @unlock()
  
  handleListeners: (action, ev)->
    slideshows[action + 'EventListener'](ev, @_handleSwipe)
  
  customSwipe: (action)->
    ['swipeleft', 'swiperight'].forEach(@handleListeners.bind(@, action))
  
  getStep: (forward)-> +forward or -1
  
  getNextSlide: (forward)->
    slides = @generateCustomTrackPath()
    slides[@getIndex(slides, @getCurrentSlide()) + @getStep(forward)]
  
  isOnPosition: (position)->
    @['isOn' + position](@generateCustomTrackPath(), @getCurrentSlide())
  
  swipeCondition: (position, direction)->
    @isOnPosition(position) and direction
  
  isGoingOutLeft: (forward)->
    @swipeCondition('Start', not forward)
  
  isGoingOutRight: (forward)->
    @swipeCondition('End', forward)
  
  isExiting: (forward)->
    @isGoingOutLeft(forward) or @isGoingOutRight(forward)
  
  navigateOnCustomTrack: (forward)->
    return if @isRouteDisabled(forward)
    if @isExiting forward
      @navigateOnSwipe forward
    else
      @navigateToCustomSlide forward
  
  navigateOnSwipe: (forward)->
    if forward then @navigate('next') else @navigate('prev')
  
  navigate: (direction)-> app.slideshow[direction]()
  
  navigateToCustomSlide: (forward)->
    app.goTo @getNavigationPath(@getNextSlide(forward))
  
  isOnRouter: ->
    @getCurrentSlide() is @getRouter()
  
  isRouteDisabled: (forward)->
    forward and @getRouter() and @isOnRouter() and not @getCustomTrack()
  
  isPresent: (arr = [], el)-> arr.indexOf(el) > -1
  
  isOnStart: (arr = [], el)-> arr.indexOf(el) is 0
  
  isOnEnd: (arr = [], el)-> arr.indexOf(el) is (arr.length - 1)
  
  getIndex: (arr = [], el) -> arr.indexOf el
  
  getCollection: -> app.slideshow.resolve().slideshow
  
  getSlideshow: (slide)-> @slideshows[@currentCollection][slide]
  
  getCurrentSlide: -> app.slideshow.get()
  
  getNavigationPath: (slide)-> @currentCollection + '/' + @getSlideshow(slide) + '/' + slide
  
  getCollectionModel: -> @structures[@currentCollection]
  
  getPathPart: (position)-> @currentCollectionModel[position]
  
  getCustomTracks: -> @currentCollectionModel?.tracks
  
  getCustomTrackSlides: -> @getCustomTracks()?[@getTrack()]
  
  getDefaultTrack: -> @currentCollectionModel.defaultTrack
  
  getRouter: -> @currentCollectionModel.pathRouter
  
  isOnCustomSlide: -> @isPresent @getCustomTrackSlides(), @getCurrentSlide()
  
  generateCustomTrackPath: ->
    path = @getCustomTrackSlides()
    pathStart = @getPathPart('pathStart')
    path = [pathStart].concat(path) if pathStart
    pathEnd = @getPathPart('pathEnd')
    path = path.concat pathEnd if pathEnd
  
  isOnCustomTrackPath: ->
    @isPresent @generateCustomTrackPath(), @getCurrentSlide()
  
  getCustomTrack: -> @customTracks[@currentCollection]
  
  getCustomTrackFromStorage: ->
    @storage.getStorageData @currentCollection
  
  getTrack: -> @getCustomTrack() or @getCustomTrackFromStorage() or @getDefaultTrack()
  
  setTrack: (data)->
    collection = data.id or @currentCollection
    track = data.track
    @customTracks[collection] = track
    @storage.save @customTracks
    app.trigger 'set:dynamic-track', track
  
  resetTrack: -> @setTrack {track: @getDefaultTrack()}
  
  clearTrack: -> @setTrack {track: ''}
  
  onRender: ->
    @storage = app.module.get @props.STORAGE
    @locker = app.module.get @props.LOCKER
    @history = app.module.get @props.HISTORY
    @init()


options = {
  slideshows:
    c1_2019_pedvt_collection:
      'PEDVTPatientProfilesSlide': 'pedvt_patient_profiles_slideshow'

      'PEDVTCATUnmetNeedASlide': 'pedvt_cat_unmet_need_a_slideshow'
      'PEDVTCATUnmetNeedBSlide': 'pedvt_cat_unmet_need_b_slideshow'
      'PEDVTCATEfficacySafetySlide': 'pedvt_cat_efficacy_safety_slideshow'
      'PEDVTNOACAttributesSlide': 'pedvt_noac_attributes_slideshow'
      'PEDVTCATSummarySlide': 'pedvt_cat_summary_slideshow'
      'PEDVTPADReminderSlide': 'pedvt_reminder_1_slideshow'

      'PEDVTHighRiskPatientsSlide': 'pedvt_high_risk_patients_slideshow'
      'PEDVTRenalSafetySlide': 'pedvt_renal_safety_slideshow'
      'PEDVTRenalEfficacySlide': 'pedvt_renal_efficacy_slideshow'
      'PEDVTXareltoBlendingProfileSlide': 'pedvt_xarelto_blending_profile_slideshow'
      'PEDVTRenalDosingSlide': 'pedvt_renal_dosing_slideshow'
      'PEDVTRenalSummarySlide': 'pedvt_renal_summary_slideshow'
      'PEDVTPADReminder2Slide': 'pedvt_reminder_2_slideshow'

      'PEDVTTreatmentTimelineSlide': 'pedvt_treatment_timeline_slideshow'
      'PEDVTVTEEfficacySlide': 'pedvt_vte_efficacy_slideshow'
      'PEDVTVTEXareltoBlendingProfileSlide': 'pedvt_vte_xarelto_blending_profile_slideshow'
      'PEDVTVTEDosingSlide': 'pedvt_vte_dosing_slideshow'
      'PEDVTXareltoDoseFlexibilitySlide': 'pedvt_xarelto_dose_flexibility_slideshow'
      'PEDVTVTESummarySlide': 'pedvt_vte_summary_slideshow'
      'PEDVTPADReminder3Slide': 'pedvt_reminder_3_slideshow'

    c1_2019_pedvt_chatbot_collection:
      'PEDVTPatientProfilesSlide': 'pedvt_chatbot_patient_profiles_slideshow'
      'PEDVTCATUnmetNeedASlide': 'pedvt_chatbot_cat_unmet_need_a_slideshow'
      'PEDVTCATUnmetNeedBSlide': 'pedvt_chatbot_cat_unmet_need_b_slideshow'
      'PEDVTCATEfficacySafetySlide': 'pedvt_chatbot_cat_efficacy_safety_slideshow'
      'PEDVTNOACAttributesSlide': 'pedvt_chatbot_noac_attributes_slideshow'
      'PEDVTCATSummarySlide': 'pedvt_chatbot_cat_summary_slideshow'

  structures:
    c1_2019_pedvt_collection:
      pathRouter: 'PEDVTPatientProfilesSlide'
      pathStart: 'PEDVTPatientProfilesSlide'
      pathEnd: 'PEDVTPatientProfilesSlide'
      tracks:
        'robert': [
          'PEDVTCATUnmetNeedASlide'
          'PEDVTCATUnmetNeedBSlide'
          'PEDVTCATEfficacySafetySlide'
          'PEDVTNOACAttributesSlide'
          'PEDVTCATSummarySlide'
          'PEDVTPADReminderSlide'
        ]
        'helen': [
          'PEDVTHighRiskPatientsSlide'
          'PEDVTRenalSafetySlide'
          'PEDVTRenalEfficacySlide'
          'PEDVTXareltoBlendingProfileSlide'
          'PEDVTRenalDosingSlide'
          'PEDVTRenalSummarySlide'
          'PEDVTPADReminder2Slide'
        ]
        'derek': [
          'PEDVTTreatmentTimelineSlide'
          'PEDVTVTEEfficacySlide'
          'PEDVTVTEXareltoBlendingProfileSlide'
          'PEDVTVTEDosingSlide'
          'PEDVTXareltoDoseFlexibilitySlide'
          'PEDVTVTESummarySlide'
          'PEDVTPADReminder3Slide'
        ]
    c1_2019_pedvt_chatbot_collection:
      pathRouter: 'PEDVTPatientProfilesSlide'
      pathStart: 'PEDVTPatientProfilesSlide'
      pathEnd: 'PEDVTPatientProfilesSlide'
      tracks:
        'robert': [
          'PEDVTCATUnmetNeedASlide'
          'PEDVTCATUnmetNeedBSlide'
          'PEDVTCATEfficacySafetySlide'
          'PEDVTNOACAttributesSlide'
          'PEDVTCATSummarySlide'
          'PEDVTPADReminderSlide'
        ]
        'helen': [
        ]
        'derek': [
        ]
}

app.register 'ah-dynamic-tracks', -> new AhDynamicTracks(options)
