# ah-dynamic-tracks
-------------------------------------
### Description
##### This module allows to create custom tracks with possibility to navigate on them.
-------------------------------------

### How to use:

##### Include module to presentation.json
```
"ah-dynamic-tracks":
		{
			"id": "ah-dynamic-tracks",
			"files": {
				"scripts": [
					"modules/rainmaker_modules/ah-dynamic-tracks/ah-dynamic-tracks.coffee"
				]
			}
		}
 ```
##### Include module to index.jade/index.pug
```
#ahDynamicTracks(data-module='ah-dynamic-tracks')
```

##### Dependencies
    You need to use the newer versions of the modules:
    - "ah-locker": ">=1.0.0"
    - "ah-correct-events": ">=1.1.0"

##### Example of using:

In slide template:
```
button.interactive(data-dynamic-track='example_track')
```

In slide instance:
```
    @events = {
      'tap .interactive': 'setDynamicTrack'
    }
    setDynamicTrack: (event)->
      app.module.get('ahDynamicTracks').setTrack {track: event.delegateTarget.dataset.dynamicTrack}
```

In module:
 - Add keys/values for slides with corresponding slideshows (we must do this because in Veeva Wide mode `presentation.json` contains only single slideshow with current slide)

 I.e.:

 ```
    @slideshows = {
          'PEDVTVTECustomisationSlide': 'pe_dvt_vte_customisation_slideshow'
          'PEDVTVTECancerTreatmentSlide': 'pe_dvt_vte_cancer_treatment_slideshow'
          'PEDVTVTEEffectiveVTEProtectionSlide': 'pe_dvt_vte_effective_vte_protection_slideshow'
          'PEDVTVTEWithoutAWarningSlide': 'pe_dvt_vte_without_awarning_slideshow'
          ...
        }
 ```

 - Describe dynamic track structure:

```
    @structures =
      start_collection:
        pathRouter: 'RouterSlide'
        pathStart: 'StartSlide'
        pathEnd: 'EndSlide'
        defaultTrack: 'example_track'
        tracks:
          'example_track': [
            'PEDVTVTECancerTreatmentSlide'
            'PEDVTVTEEffectiveVTEProtectionSlide'
            'PEDVTVTEWithoutAWarningSlide'
            'PEDVTVTESingleDrugApproachSlide'
            'PEDVTVTEProtectionSlide'
            'PEDVTVTETailoredProtectionRobertSlide'
          ]
          'another_track': [
            'PEDVTVTEHighestNumberSlide'
            'PEDVTVTEConsistentVTEPRotectionSlide'
            'PEDVTVTELowerMajorBleedingSlide'
            'PEDVTVTEConsistentSafetyProfileSlide'
            'PEDVTVTETailoringDosageSlide'
            'PEDVTVTETailoredProtectionHelenSlide'
          ]
```
```pathRouter``` - slide which blocks further navigation until dynamic track is chosen

```pathStart``` - [optional] static start of flow

```pathEnd``` - [optional] static end of flow

```defaultTrack``` - [optional] fallback dynamic track which will be used if nothing was changed via module API

-------

### API

```setTrack(data)``` - sets dynamic track

```data = {id: collectionId, track: trackName}```:
 - ```id``` - optional - collection id
 - ```track``` - track id (i.e. ```another_track```)

```resetTrack``` - resets track for current collection to ```defaultTrack``` value