# ah-check-clm-system
### This module checks which environment (CLM system) you are currently in

## How to add to the project:  
  Copy folder `ah-check-clm-system` to `app/modules/rainmaker_modules` then add this markup `div(data-module="ah-check-clm-system")` to `index.pug`.
  
Add links to `platforms/rainmaker/presentation.json` to modules object: 
```        
"ah-check-clm-system": {
  "id": "ah-check-clm-system",
  "files": {
    "scripts": [
      "modules/rainmaker_modules/ah-check-clm-system/ah-check-clm-system.coffee"
    ]
  }
}
```
## How to use:
These props are available from global `app` object:
- isiPlanner
- isRainmaker
- isCegedim 
- isVeevaDeep
- isVeevaWide
- isSpa

`app.isVeevaDeep` and `app.isVeevaWide` is set to `true` during build process.

1.1.0 - `app.isVeeva` is deprecated. Use `app.isVeevaDeep` instead.