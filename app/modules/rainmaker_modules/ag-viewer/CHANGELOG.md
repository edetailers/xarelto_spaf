### Version 1.2.1 2018-03-26
- Add functionality adding custom class for slideshow in ag-viewer. You need to add attribute 'data-custom-class=className' to open button.
- Change event "click" to "tap"
