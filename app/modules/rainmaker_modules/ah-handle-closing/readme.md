# ah-handle-closing
-------------------------------------
### Description
##### This module close inline-slideshow before opening popup
-------------------------------------

### How to use:

##### Include module to presentation.json
```
"ah-handle-closing": {
            "id": "ah-handle-closing",
            "files": {
                "scripts": [
                    "modules/rainmaker_modules/ah-handle-closing/ah-handle-closing.coffee"
                ]
            }
 }
 ```    
##### Include module to index.jade/index.pug
```
div(data-module='ah-handle-closing')
```

##### Dependencies
    You need to use the newer versions of the modules:
    - "ah-inline-slideshow": ">=1.0.0"
    - "ag-overlay": ">=0.5.0"
    - "ap-toolbar"

##### Example of using - use when you need to close inline-slideshow before opening popup

## Changelog

### Version 1.0.1
### 2019-02-12 - A.Zhdaniuk
- add possibility for close inline-slideshow before opening any popup