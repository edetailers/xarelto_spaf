# ah-inline-slideshow
-------------------------------------
### Description
##### This module allows you to create popup of set of slides with own navigation.
##### You also have a possibility to add controls buttons.
-------------------------------------

### How to use:

##### Include module to presentation.json
```

"ah-inline-slideshow": {
    "id": "ah-inline-slideshow",
    "files": {
        "scripts": [
            "modules/rainmaker_modules/ah-inline-slideshow/ah-inline-slideshow.coffee"
        ],
        "styles": [
            "modules/rainmaker_modules/ah-inline-slideshow/ah-inline-slideshow.styl"
        ]
    }
}
 ```    
##### Include module to index.jade/index.pug
```
#inlineSlideshow(data-module='ah-inline-slideshow')
```

##### Dependencies
    You need to use the newer versions of the modules:
    - "ah-inline-indicators": ">=1.0.0"
    - "ag-viewer"

##### Example of using - use some button for transition to slideshow.

```
button.interactive(data-viewer='slideshow' href='test_inline_slideshow' slide='firstSlide')
```
"test_inline_slideshow": {
    "name": "test inline_slideshow",
    "id": "test_inline_slideshow",
    "type": "slideshow",
    "content": [
        "firstSlide",
        "secondSlide",
        "thirdSlide"
    ]
}
```
