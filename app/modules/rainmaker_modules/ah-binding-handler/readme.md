# ah-binding-handler
##### This module adds data attributes event handlers on slide enter
#### Version 1.0.1 (05.10.2017, Oleksandr Tkachov):
##### Now module uses `ah-history` for changing slide/slide-popup states
#### Update 31.08.2017:
##### Now module uses `ah-history` module event `view-enter` for monitoring. Add inline slideshow opening binding.
### How to use:

##### Include module to presentation.json
```

"ah-binding-handler": {
    "id": "ah-binding-handler",
    "files": {
        "scripts": [
            "modules/rainmaker_modules/ah-binding-handler/ah-binding-handler.js"
        ]
    }
}
 ```    
##### Include module to index.jade/index.pug
```
#ahBindingHandler(data-module='ah-binding-handler')
```

##### Dependencies
    You need to use the newer versions of the modules:
    - "ah-history": ">=2.0.0"

##### Example of using - use some button for transition to slideshow.

```    
    Open popup on element tap:
        div(data-popup="PopupSlideId" data-overlay-id="OveralyInstanceId")
    Change slide state on element tap:
        div(data-goto-state="animate")
    Go to other slide on element tap:
        div(data-goto-slide="SlideId")
    Disable swipe on definite element:
        div(data-stop-swipe)
    Open inline slideshow on element tap:
        div(data-inline-slideshow='slideshow' slide='slideId')
        
## Changelog

*   v.1.1.1
        2018-10-01
            update:
                add attribute data-overlay-id to specify overaly module instance for popups
                
*   v.1.1.0
        2018-06-28
            update:
                checking slide data before applying bindings, passing overlayid through props

*   v.1.0.3
        2018-03-13 - Oleksandr Melnyk
            bugfix:
                correct component name at including module

*   v.1.0.2
        2018-03-05 - Oleksandr Melnyk
            refactoring:
                fix JSHint warnings
