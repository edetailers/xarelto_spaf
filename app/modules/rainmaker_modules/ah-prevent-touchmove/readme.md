# ah-prevent-touchmove
##### This module prevents default actions on touchmove event on all elements except elements in 'excludingElements' list

#### Version 0.1.0

### How to use:
##### Include module to index.jade/index.pug
```
div(data-module='ah-prevent-touchmove')
```