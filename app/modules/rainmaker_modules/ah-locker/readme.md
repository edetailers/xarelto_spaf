# ah-locker
-------------------------------------
### Description
##### This module allows to manipulate lock/unlock methods.
-------------------------------------
--------------------------------

### How to use:

##### Include module to presentation.json
```
"ah-locker":
		{
			"id": "ah-locker",
			"files": {
				"scripts": [
					"modules/rainmaker_modules/ah-locker/ah-locker.coffee"
				]
			}
		}
 ```
##### Include module to index.jade/index.pug
```
#ahLocker(data-module='ah-locker')
```

##### Example of using:

```
app.module.get('ahLocker').appLock()
```