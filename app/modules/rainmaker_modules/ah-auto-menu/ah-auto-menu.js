function ahAutoMenu() {
	var t = 'ag-auto-menu',
		r = {},
		a = '',
		n = ['NavigationSlide'],
		o = null;
	if (app.registry.exist(t)) {
		var s = app.registry.get(t);
		s.publish.hide = !0, s.updateCurrent = function (t) {
			this.current = s.updateActiveItem.call(this), this.goTo('hidden'), s.disableMenu.call(this, t), setTimeout(function () {
				(app.isVeevaWide || app.isVeevaDeep) && this.el.classList.add('veeva-shift');
			}.bind(this), 0);
		}, s.disableMenu = function (t) {
			var e = t || {
				current: {
					id: app.slideshow.resolve().slide
				}
			};
			n.indexOf(e.current.id) > -1 && this.goTo('disabled');
		}, s.updateActiveItem = function () {
			var t = app.slideshow.getId();
			s.removeActiveItems.call(this);
			var e = s.setActiveItem.call(this, 'data-item', t);
			return e ? [e] : [];
		}, s.removeActiveItems = function () {
			this.current.length > 0 && this.current.forEach(function (t) {
				t.classList.remove('active');
			});
		}, s.setActiveItem = function (t, e) {
			var n = this.el ? this.el.querySelector('.menu [' + t + '="' + e + '"]') : null;
			return n && n.classList.add('active'), n;
		}, s.events = {
			tap: function () {
				this.toggle('hidden'), s.toolbar.hide();
			},
			'tap li[data-item]': 'navigate',
			swipeleft: function (t) {
				t.stopPropagation();
			},
			swiperight: function (t) {
				t.stopPropagation();
			}
		}, s.states = [{
			id: 'hidden',
			onEnter: function () {
				s.disableMenu.call(this);
			},
			onExit: function () {}
		}, {
			id: 'disabled',
			onEnter: function () {},
			onExit: function () {}
		}], s.regenerateMenu = function (t) {
			var e = t.collectionId,
				n = t.country;
			s.setup.call(this, e, n), s.updateCurrent.call(this);
		}, s.setup = function (t, e) {
			o = app.module.get('ahVeevaStorageData'), s.toolbar = app.module.get('apToolbar'), r = s.getStructures(), a = e || s.getCountry(t), s.generateMenu.call(this, a), app.on('menu:highlight-item', function (t) {
				this.current = s.updateActiveItem.call(this);
				var e = s.setActiveItem.call(this, 'data-state', t);
				e && this.current.push(e);
			}.bind(this));
		}, s.getPath = function (t) {
			var e = r.storyboards[t].content[0],
				n = r.structures[e].content[0],
				a = t;
			return e && n && (a += '/' + e + '/' + n), a;
		}, s.navigate = function (t) {
			t.stopPropagation();
			var e = t.target,
				n = e ? s.getPath(e.getAttribute('data-item')) : '',
				a = e ? e.getAttribute('data-state') : '',
				i = s.getSlideIdFromPath(n);
			if (this.nextActiveElement = e, a) {
				var r = {};
				r[i] = a, o.add('slidesState', r), app.trigger('update:slide-state', {
					slideId: i,
					state: a
				});
			}
			n && app.goTo(n);
		}, s.getSlideIdFromPath = function (t) {
			var e = t.split('/');
			return e[e.length - 1];
		}, s.layout = function (t) {
			this.el && (void 0 === this.el.style.zoom || navigator.userAgent.match(/(iphone|ipod|ipad|android)/gi) || (this.el.style.zoom = t.scale));
		}, s.disable = function () {
			this.el.classList.add('disabled');
		}, s.enable = function () {
			this.el.classList.remove('disabled');
		}, s.getCountry = function (t) {
			var e = t || app.slideshow.getId(),
				n = r.storyboards[e].start;
			return n || console.error('ah-auto-menu: field country doesn\'t setup!!!'), n;
		}, s.generateMenu = function (t) {
			var e = this.el.getElementsByClassName('menu')[0];
			e.innerHTML = '';
			var n = document.createDocumentFragment(),
				a = s.getMenuCollections(t);
			a.length > 0 ? (n.appendChild(s.generateHome()), Object.keys(r.products).forEach(function (t) {
				var e = s.generateProductItem(a, t);
				e && n.appendChild(e);
			}), e.appendChild(n), s.enable.call(this)) : s.disable.call(this);
		}, s.getMenuCollections = function (t) {
			var n = t || a;
			return Object.keys(r.storyboards).filter(function (t) {
				var e = r.storyboards[t];
				return e.country.indexOf(n) > -1 && e.start !== n || e.product;
			});
		}, s.generateHome = function () {
			var t = Object.keys(r.storyboards).find(function (t) {
				return r.storyboards[t].start === 'core-spaf-cardio';
			});
			return s.generateItem(t, 'menu-home', !0);
		}, s.generateItem = function (t, e, n) {
			var a = document.createElement('li');
			return a.setAttribute('data-item', t), e && a.classList.add(e), n || (a.innerHTML = r.storyboards[t].name), a;
		}, s.generateProductItem = function (t, e) {
			var n = null,
				a = s.generateProductSubItems(t, e);
			return a && ((n = document.createElement('li')).appendChild(s.generateProductTitle(e)), n.appendChild(a)), n;
		}, s.generateProductTitle = function (t) {
			var e = document.createElement('p');
			return e.innerHTML = r.products[t], e;
		}, s.generateProductSubItems = function (t, e) {
			var n, a, i = document.createElement('ul');
			return t.forEach(function (t) {
				a = r.storyboards[t].product, (n = a ? a.split(',') : null) && n.indexOf(e) > -1 && i.appendChild(s.setState(s.generateItem(t), t, e));
			}), i.childElementCount > 0 ? i : null;
		}, s.setState = function (t, e, n) {
			var a = r.storyboards[e].states,
				i = a ? a[n] : null;
			return i && t.setAttribute('data-state', i), t;
		}, s.getStructures = function () {
			return app.isVeevaWide ? JSON.parse(window.app.cache.get('presentation.json')) : app.model.get();
		};
	} else console.error('ah-auto-menu: ag module instance doesn\'t exist');
}
app.on('presentation:ready', ahAutoMenu);
