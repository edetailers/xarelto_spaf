ah-veeva-api
=========

Module for work with Veeva API.

## Usage

#### Include module to presentation.json

```

"ah-veeva-api": {
    "id": "ah-veeva-api",
    "files": {
        "scripts": [
            "modules/rainmaker_modules/ah-veeva-api/ah-veeva-api.coffee"
        ]
    }
}

 ```   
 



#### 2) Use the following functions:


- ```getAccountId()```  returns the promise with AccountId
- ```getPresentationId()``` returns the promise with PresentationId
- ```getDataForCurrentObject: (object, field)``` returns the promise, it is the wrapper on the standard function  com.veeva.clm.getDataForCurrentObject
- ```openMedia(filename, type)``` to open the media file. type:(pdf, video)