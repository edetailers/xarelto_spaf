# ah-monitoring-tracker module
-------------------------------------
### Description
##### This module trigger event for monitoring and based on view events(view-exit, view-enter).
-------------------------------------

### How to use:

##### Include module to presentation.json
```
"ah-monitoring-tracker": {
            "id": "ah-monitoring-tracker",
            "files": {
                "scripts": [
                    "modules/rainmaker_modules/ah-monitoring-tracker/ah-monitoring-tracker.coffee"
                ]
            }
        },
 ```    
##### Include module to index.jade/index.pug
```
#monitoringTracker(data-module='ah-monitoring-tracker')
```

##### Dependencies
    Modules:
         - ah-history v2.0.0

##### Excluded slides: you can add excludes for monitoring slides in presentation.json, 
if slide have property "isExcluded" it will not be tracked, for example:

```
  "slides": {
          "CoverSlide": {
              "id": "CoverSlide",
              "name": "Cover – Version 1",
              "isExcluded": true,
              "files": {
                  "templates": [
                      "slides/CoverSlide/CoverSlide.pug"
                  ],
                  "scripts": [
                      "slides/CoverSlide/CoverSlide.coffee"
                  ],
                  "styles": [
                      "slides/CoverSlide/CoverSlide.styl"
                  ]
              }
          },
```

##### Standard monitoring: 
On view-enter, view-exit events, module automatically trigger events for your monitoring module. In callback we have info about current view (id,type...)

```
app.trigger('monitoringTracker:start', data)
app.trigger('monitoringTracker:submit', data)

```
##### Custom monitoring: 
For custom monitoring you can use "submitCustom" function on slide:
```
  app.module.get('monitoringTracker').submitCustom(data); 
```
This function trigger event for your monitoring module:
 
```
  app.trigger('monitoringTracker:submit-custom', data)
```