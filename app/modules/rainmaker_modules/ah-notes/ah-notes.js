(function () {
	app.register('ah-notes', function () {
		return {
			publish: {
				useGlobal: !1,
				attr: 'data-notes',
				injectAttr: 'data-inject',
				spaceCharacter: '&nbsp;',
				separator: ' ',
				separateValue: 1,
				useLatinNumbers: !0,
				startIndexLetter: 64,
				parseNumberLetter: 26,
				// replace this with custom alphabet
				alphabet: {
					'a': 'a',
					'b': 'b',
					'c': 'c',
					'd': 'd',
					'e': 'e',
					'f': 'f',
					'g': 'g',
					'h': 'h',
					'i': 'i',
					'j': 'j',
					'k': 'k',
					'l': 'l',
					'm': 'm',
					'n': 'n',
					'o': 'o',
					'p': 'p',
					'q': 'q',
					'r': 'r',
					's': 's',
					't': 't',
					'u': 'u',
					'v': 'v',
					'z': 'z'
				}
			},
			events: {
				'tap .ag-overlay-x': 'hide'
			},
			states: [{
				id: 'show'
			}],
			loadedData: null,
			_renderedNotes: {},
			getCurrentSlide: function () {
				var t;
				return (t = app.slide.get(app.slideshow.get())) != null ? t.el : void 0;
			},
			onRender: function () {
				return this.injectedNotes = {}, this.$root = $('#notes'), this.$list = this.$root.find('ul'), this._preventSwipe(), this._dataSetPropName = this._getDatasetProp(), this.init(), this.scroll && this.scroll.destroy(), this.scroll = new IScroll($('.scroll')[0], {
					scrollbars: !1,
					bounce: !1
				});
			},
			handleEnter: function (t) {
				var e;
				if (this.resetList(), e = this._renderedNotes[t.id]) {
					return console.log(e), console.log('injected', this.injectedNotes), app.trigger('notes:update', {
						list: e
					});
				}
			},
			handleRender: function (t) {
				return this._$el = $('.slide[id=' + t.id + ']'), this._$el.length && this.updateNotes(this._$el), app.trigger('notes:render', {
					list: this._renderedNotes[t.id],
					slideId: t.id
				});
			},
			handleUpdate: function (t) {
				var e;
				if (e = this.getCurrentSlide()) {
					return app.trigger('notes:update', {
						list: this._renderedNotes[e.id]
					});
				}
			},
			handleLeave: function () {},
			handleCloseOverlay: function () {
				var t;
				return (t = {}).id = app.slideshow.get(), this.handleEnter(t);
			},
			_preventSwipe: function () {
				return this.$root.on('swipedown swipeup swiperight swipeleft', function (t) {
					return t.stopPropagation();
				});
			},
			show: function () {
				if (this.$list.find('li').length) return this.goTo('show');
			},
			hide: function () {
				return this.reset();
			},
			init: function () {
				return this.loadedData = window.ahstrings.notes, this.handleRender({
					id: app.slideshow.get()
				}), app.listenTo(app.slide, 'slide:anthill-render', this.handleRender.bind(this)), app.listenTo(app.slideshow, 'update:current', this.handleUpdate.bind(this)), app.listenTo(app, 'view-enter', this.handleEnter.bind(this)), app.listenTo(app, 'view-exit', this.handleLeave.bind(this));
			},
			_fiterUnique: function (t) {
				return t.filter(function (t, e, n) {
					return n.indexOf(t) === e;
				});
			},
			_isAbbreviation: function (t) {
				return this.loadedData[t].isAbbreviation;
			},
			_parseDOM: function (t) {
				var e, r, i, s, o;
				return s = [], i = [], r = t[0].id, t.find('[' + this.props.attr + ']').each((o = this, function (t, e) {
					var n;
					return e.getAttribute(o.props.injectAttr) && o._findInjectedNotes(e, r), (n = o._getNotesKey(e)).length || o._throwException('Invalid notes in ' + r), s.push(n.trim()), i.push(e);
				})), this.notesKeys = s.length ? this._fiterUnique(this._devideArrray(s)) : [], this.notesKeys = this.sortNotes(this.notesKeys), e = this._getBindedData(this.notesKeys, i), this._setCounters(e), this._renderedNotes[r] = this.notesKeys, this.injectNotes(r), this.notesKeys;
			},
			sortNotes: function (t) {
				var e, n, r;
				return n = [], e = t.filter((r = this, function (t) {
					var e;
					return (e = r.loadedData[t].isAbbreviation) || n.push(t), e;
				})), n.concat(e);
			},
			getInjectedNotes: function (t) {
				return this.injectedNotes[t] ? this.injectedNotes[t].slice() : [];
			},
			_findInjectedNotes: function (t, e) {
				return this.injectedNotes[e] = (this.injectedNotes[e] || []).concat(t.getAttribute(this.props.attr).split(' ')).filter(function (t, e, n) {
					return n.indexOf(t) === e;
				});
			},
			_throwException: function (t) {
				throw new Error(t);
			},
			_getBindedData: function (t, e) {
				var i;
				return e.map((i = this, function (n) {
					var r;
					return r = [], t.forEach(function (t, e) {
						if (i._contains(i._getNotesKey(n), t)) {
							return r.push({
								key: t,
								index: i.props.useGlobal ? i._getAbsoluteCounter(t) : e
							});
						}
					}), {
						element: n,
						containedNotes: r
					};
				}));
			},
			_setCounters: function (t) {
				return t.forEach((r = this, function (t) {
					var e, n;
					return e = r._getCounters(t.containedNotes), n = r._getSequence(e), t.element.textContent = n.toString();
				}));
				var r;
			},
			resetList: function () {
				return this.$list.empty();
			},
			updateNotes: function (t) {
				return this._parseDOM(t);
			},
			injectNotes: function (t) {
				var e, n, r;
				if (this.injectedNotes[t] && (e = this.injectedNotes[t].map((r = this, function (t) {
					var e;
					return e = r._isAbbreviation(t) ? '' : '<sup>' + r._getNumberKey(t) + '</sup>', $('<p>' + e + r.loadedData[t].description + r.props.spaceCharacter + '</p>');
				}))), e) return (n = $('<div class=\'injected-notes\'></div>')).append(e), this._$el.append(n);
			},
			_getNumberKey: function (t) {
				return this._generateLetterOrNumber(this.notesKeys.indexOf(t) + 1);
			},
			_getSequence: function (t) {
				var u, d;
				return u = [], t.reduce((d = this, function (t, e, n, r) {
					var i, s, o, a;
					if (e === r[n + 1] - 1) return ++t;
					if (t > d.props.separateValue) u.push(d._generateLetterOrNumber(r[n - t]) + '-' + d._generateLetterOrNumber(r[n]));
					else {for (i = s = o = n - t, a = n; o <= a ? s <= a : a <= s; i = o <= a ? ++s : --s) u.push(d._generateLetterOrNumber(r[i]));}
					return 0;
				}), 0), u;
			},
			_generateLetterOrNumber: function (t) {
				if ( this.props.alphabet ) {
					var key = String.fromCharCode(this.props.startIndexLetter + t % this.props.parseNumberLetter).toLowerCase();
					return this.props.alphabet[ key ];
				}
				return this.props.useLatinNumbers ? String.fromCharCode(this.props.startIndexLetter + t % this.props.parseNumberLetter).toLowerCase() : t;
			},
			_getCounters: function (t) {
				return t.map(function (t) {
					return ++t.index;
				});
			},
			_getNotesKey: function (t) {
				return t.dataset[this._dataSetPropName];
			},
			_devideArrray: function (t) {
				return t.join(this.props.separator).split(this.props.separator);
			},
			_contains: function (t, e) {
				return new RegExp('\\b' + e + '\\b', 'g').test(t);
			},
			_getAbsoluteCounter: function (t) {
				return Object.keys(this.loadedData).indexOf(t);
			},
			_getDatasetProp: function () {
				return this.props.attr.slice(5, this.props.attr.length).replace(/-([a-z])/g, function (t) {
					return t[1].toUpperCase();
				});
			}
		};
	});
}).call(this);
