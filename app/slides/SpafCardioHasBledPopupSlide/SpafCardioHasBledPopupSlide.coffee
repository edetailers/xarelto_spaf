
class SpafCardioHasBledPopupSlide
  constructor: ()->
    @events = {}
    @states = [
      {id: 'default'}
      {id: 'animate'}
    ]
#    @slideStrings = '%=slideStrings%'

  onRender: (element)->

  onEnter: (element)->
    @goTo 'animate'

  onExit: (element)->
    @reset()

app.register('SpafCardioHasBledPopupSlide', -> new SpafCardioHasBledPopupSlide())
