
class SpafCardioCoverSlide
  constructor: ()->
    @events = {
      'tap video': @runVideo,
      'swipeleft .video-wrapper': @stopSwipe
      'swiperight .video-wrapper': (event)->
        event.stopPropagation() if app.isVeevaWide
    }
    @states = [
      {id: 'default'},
      {id: 'frame1'},
      {id: 'frame2'}
    ]

  onRender: (element)->
    @storageData = app.module.get 'ahVeevaStorageData'
    @storageKey = app.slideshow.get() + '_FastSkipVideo'
    app.on 'skipVideo', @fastSkip.bind(@)
    @element = element
    @videoElement = element.getElementsByClassName("cover-video")[0]
    @videoElement.addEventListener('ended', @preExit.bind(@))
    @videoElement.loop = false
    @videoElement.load()

  preExit: ()->
    @goTo('frame1')
    clearTimeout(@resizeTimeout)
    @resizeTimeout = setTimeout(()=>
      @goTo('frame2')
      @removeVideo()
    , 2000)

  runVideo: (e)->
    if @videoElement
      @el.classList.add('hide-play')
      @videoElement.play()

  stopSwipe: (e)->
    @videoElement.pause()
    e.stopPropagation()
    @fastSkip(true)

  removeVideo: (e)->
    if !@el.classList.contains('hide-element') then @el.classList.add('hide-element')

  onEnter: (element)->

  fastSkip: (skip)->
    currentStorageKey = @storageData.getStorageData(@storageKey)
    if skip or currentStorageKey
      @removeVideo()
      @element.classList.add('skip-animation')
    else
      @storageData.add(@storageKey, true)

  onExit: ()->
    @fastSkip(false)

app.register('SpafCardioCoverSlide', -> new SpafCardioCoverSlide())
