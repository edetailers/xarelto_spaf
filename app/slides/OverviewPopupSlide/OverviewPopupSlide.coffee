class OverviewPopupSlide
  constructor: ()->
    @events = {}
    @states = []

  slidesMap = undefined

  onRender: (element)->

  getSlideshows: (collection)->
    app.model.get().storyboards[collection].content

  getSlides: (slideshows)->
    slides = []
    slideshows.map (slideshow)->
      app.model.get().structures[slideshow].content
    .forEach (slideshowSlides)->
      slides = slides.concat slideshowSlides
    slides

  createButton: (item)->
    button = document.createElement('button')
    button.classList.add(item)
    button.style.backgroundImage = 'url("slides/' + item + '/' + item + '.png")'
    button.addEventListener "tap", ->
      app.slideshow.goTo item
      app.view.get('overlayPopupCustom').close()
    button

  build: (event)->
    slides = @getSlides @getSlideshows app.slideshow.resolve().slideshow
    slidesMap.innerHTML = ''
    slides.forEach (item, index)=>
      slidesMap.appendChild(@createButton item)

  onEnter: (element)->
    slidesMap = element.querySelector '.slides-map'
    this.build()
    current = app.slide.get().id
    slide = slidesMap.getElementsByClassName(current)[0]
    prevActive = slidesMap.getElementsByClassName('active')[0]
    if @scroll
      @scroll.destroy()
    @scroll = new IScroll($(".scroll")[0], scrollbars: true)

    if prevActive
      prevActive.classList.remove 'active'
    if slide
      slide.classList.add 'active'

  onExit: (element)->

app.register('OverviewPopupSlide', -> new OverviewPopupSlide())