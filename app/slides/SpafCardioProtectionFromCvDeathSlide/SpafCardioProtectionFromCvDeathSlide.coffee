
class SpafCardioProtectionFromCvDeathSlide
  constructor: ()->
    @events = {}
    @states = [
      {id: 'default'}
      {id: 'animate'}
    ]
#    @slideStrings = '%=slideStrings%'

  onRender: ()->

  onEnter: ()->
    @goTo 'animate'

  onExit: ()->
    @reset()

app.register('SpafCardioProtectionFromCvDeathSlide', -> new SpafCardioProtectionFromCvDeathSlide())
