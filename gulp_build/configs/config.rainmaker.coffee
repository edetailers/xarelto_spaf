rm_json =
  platformname: "rainmaker"
  platformname_short: "rm"

  dir:
    source: './app/'
    tmp_source: './tmp/'
    test: 'tests'
    platform: 'platforms/rainmaker/'
    platform_cegedim: 'platforms/cegedim/'
    slides_root: 'slides/'
    modules_root: 'modules/rainmaker_modules/'
    veeva_modules_root: 'modules/veeva_modules/'
    shared: 'shared/'
    assets: 'assets/'
    images: 'images/'
    icons: 'images/icons/'
    global_styles: 'global_styles/'
    accelerator: 'accelerator/'
    vendor: '_vendor/'
    localizer_module: 'app/modules/rainmaker_modules/ah-localizer'
    dest: './build/'
    rainmaker_template: /rainmaker_modules/g


rm_json.dir.configs = rm_json.dir.source + 'platforms/rainmaker/config.json'
rm_json.dir.pdf_configs = rm_json.dir.source + 'pdfs-config.json'
rm_json.dir.presentation_config = 'presentation.json'
rm_json.dir.presentations_config = rm_json.dir.source + 'presentations-config.json'

rm_json.dir.json_sources = [
  rm_json.dir.source + 'platforms/rainmaker/*.json',
  rm_json.dir.source + '**/*.json',
  '!' + rm_json.dir.source + 'slides/**/',
  '!' + rm_json.dir.source + '_vendor/',
  '!' + rm_json.dir.source + 'accelerator/',
  '!' + rm_json.dir.source + 'platforms/agnitio/',
  '!' + rm_json.dir.source + rm_json.dir.platform_cegedim,
  '!' + rm_json.dir.source + 'platforms/veeva/'
]

rm_json.dir.exclude = {
  modules: [
    '**/model.json'
    '**/package.json'
    'ag-overlay/overlay.html'
  ]
  tmp: [
    '**/*.md'
    'structure-strings.json'
    'references.json'
    'contentGroups.json'
    'accelerator/version.json'
    '_vendor/jquery.easing.1.3.js'
    'accelerator/lib/bind.js'
    '_vendor/cdm.sketcher.js'
    '_vendor/jquery.ui.touch-punch.js'
    'accelerator/lib/promise.min.js'
    'accelerator/lib/agnitio.js'
  ]
  ignore: [
    "accelerator/lib/touchy.min.js"
  ]
}
rm_json.dir.bot_configs = {
  config: 'config.json'
  files: [
    {
      path: rm_json.dir.dest
      file: 'index.html'
      newName: 'detailer'
    }
  ]
}
module.exports = rm_json
