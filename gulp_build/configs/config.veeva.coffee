module.exports =
  platformname: "veeva"
  platformname_short: "vv"
  mediaFileFormats:
    pdf:
      fileType: '.pdf'
      crmMediaType: 'PDF'
      type: 'Slide'
    video:
      fileType: '.mp4'
      crmMediaType: 'Video'
      type: 'Slide'
    slide:
      fileType: '.zip'
      crmMediaType: 'HTML'
      type: 'Slide'
      isAddPrefix: true
    shared:
      fileType: '.zip'
      crmMediaType: 'HTML'
      type: 'Shared'
      isAddPrefix: false
  multichannelCSV:
    fields:[
      {
        name: 'external_id__v'
        label: 'external_id__v'
        _get:(name)->name or ''
      }
      {
        name: 'presentationLink'
        label: 'Presentation Link'
        _get:(id)-> id or ''
      }
      {
        name: 'document_id__v'
        label: 'document_id__v'
        _get:(id)-> id or ''
      }
      {
        name: 'type'
        label: 'Type'
        _get:(type)-> type or 'Presentation'
      }
      {
        name: 'lifecycle__v'
        label: 'lifecycle__v'
        _get:(isSlide)-> if isSlide then 'CRM Content Lifecycle' else 'Binder Lifecycle'
      }
      {
        name: 'name__v'
        label: 'name__v'
        _get:(name__v)-> name__v or ''
      }
      {
        name: 'slideTitle'
        label: 'slide.title__v'
        _get:(slideTitle)-> slideTitle or ''
      }
      {
        name: 'slidefilename'
        label: 'slide.filename'
        _get:(slidefilename)-> slidefilename or ''
      }
      {
        name:'productName'
        label:'slide.product__v.name__v',
        _get:(productName, isSlide)-> if isSlide then productName else ''
      }
      {
        name:'slideCountry'
        label:'slide.country__v.name__v',
        _get:(slideCountry, isSlide)-> if isSlide then slideCountry else ''
      }
      {
        name:'presLanguage'
        label:'pres.language__v',
        _get:(presLanguage, isSlide)-> if isSlide then '' else presLanguage
      }
      {
        name:'slideLanguage'
        label:'slide.language__v',
        _get:(slideLanguage, isSlide)-> if isSlide then slideLanguage else ''
      }
      {
        name:'clmContent'
        label:'slide.clm_content__v',
        _get:(isSlide)-> if isSlide then 'YES' else ''
      }
      {
        name:'engageContent'
        label:'slide.engage_content__v',
        _get:(isSlide)-> if isSlide then 'NO' else ''
      }
      {
        name:'crmMediaType'
        label:'slide.crm_media_type__v',
        _get:(crmMediaType)-> crmMediaType or ''
      }
      {
        name:'relatedSharedResource'
        label:'slide.related_shared_resource__v',
        _get:(sharedFolder, isSlide)-> if isSlide then sharedFolder else ''
      }
      {
        name:'crm_SharedResource'
        label:'slide.crm_shared_resource__v',
        _get:(CRMsharedFolder, isSlide)-> if isSlide then CRMsharedFolder else ''
      }
      {
        name:'disableActions'
        label:'slide.crm_disable_actions__v',
        _get:(disableActions, isSlide)-> if isSlide then disableActions else ''
      }
      {
        name:'customReaction'
        label:'slide.crm_custom_reaction__v',
        _get:(customReaction, isSlide)-> if isSlide then customReaction else ''
      }
      {
        name:'presentationId'
        label:'pres.crm_presentation_id__v',
        _get:(presentationId)-> presentationId or ''
      }
      {
        name:'hiddenId'
        label:'pres.crm_hidden__v',
        _get:(hiddenId)-> hiddenId or ''
      }
      {
        name:'productNamePres'
        label:'pres.product__v.name__v',
        _get:(productNamePres, isSlide)-> if isSlide then '' else productNamePres
      }
      {
        name:'countryName'
        label:'pres.country__v.name__v',
        _get:(countryName, isSlide)-> if isSlide then '' else countryName
      }
      {
        name:'content'
        label:'pres.clm_content__v',
        _get:(content, isSlide)-> if isSlide then '' else content
      }
      {
        name:'engageContentPres'
        label:'pres.engage_content__v',
        _get:(engageContentPres, isSlide)-> if isSlide then '' else engageContentPres
      }
      {
        name:'coBrowse'
        label:'pres.cobrowse_content__v',
        _get:(coBrowse, isSlide)-> if isSlide then '' else coBrowse
      }
    ],
    CFFields:[
      {
        name: 'slideDistributionGroup'
        label:'slide.distribution_group__c',
        _get:(value, isSlide)-> if isSlide then value else ''
      },
      {
        name: 'slideBusinessUnit'
        label:'slide.business_unit__c',
        _get:(value, isSlide)-> if isSlide then value else ''
      },
      {
        name: 'slideIndication'
        label:'slide.indication__c.name__v',
        _get:(value, isSlide)-> if isSlide then value else ''
      },
      {
        name: 'presDistributionGroup'
        label:'pres.distribution_group__c',
        _get:(value, isSlide)-> if isSlide then '' else value
      },
      {
        name: 'presBusinessUnit'
        label:'pres.business_unit__c',
        _get:(value, isSlide)-> if isSlide then '' else value
      },
      {
        name: 'presIndication'
        label:'pres.indication__c.name__v',
        _get:(value, isSlide)-> if isSlide then '' else value
      }
  ]
  dir:
    source: './app/'
    pdfPath: './app/shared/pdfs',
    videoPath: './app/shared/videos',
    media: './app/media.json',
    analytics: './app/analytics.json'
    platform_files:
      app_config:
        src:"./app/platforms/veeva/"
        configs: "./app/platforms/veeva/configs.json"
        key_message: "key_message_name"
        glob:["**"]
        dest:""

    build_content_folder:'./build'
    distribution_build: './dist/veeva/build/'
    distribution_media: './dist/veeva/media/'
    pdfs_path: 'shared/pdfs/'
    videos_path: 'shared/videos/'
    build_path_single:  './build/'
    dest: './dist/veeva/'
    pdf_configs: 'pdfs-config.json'
    video_configs: 'videos-config.json'
