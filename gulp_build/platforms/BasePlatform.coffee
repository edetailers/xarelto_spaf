#Builders Classes
HTMLBuilder = require './../builders/HTMLBuilder'
ScriptsBuilder = require './../builders/ScriptsBuilder'
StyleBuilder = require './../builders/StyleBuilder'
AssetsBuilder = require './../builders/AssetsBuilder'
CommonBuilder = require './../builders/CommonBuilder'
BuildCleaner = require './../builders/BuildCleaner'
PreBuildBuilder = require './../builders/PreBuildBuilder',
XMLBuilder = require './../builders/XMLBuilder'
ClusterBuilder = require './../builders/ClusterBuilder'
ConfigBuildBuilder = require './../builders/ConfigBuildBuilder'
PostBuildBuilder = require './../builders/PostBuildBuilder'

class BasePlatform
  constructor: (@config)->


  register: ()->
#Create builder instances
    console.log('\n===================================\n\tRegister builders\n===================================\n')

    #todo: Important
    # Change config for cluster build. Build from custom source
    # Should be on top execute chain
    console.log("Pre Build Build")
    @preBuildBuilder = new PreBuildBuilder(@config)
    @config = @preBuildBuilder.changeConfig(@config) if @preBuildBuilder.vw

    console.log("Cluster Builder")
    @clusterBuilder = new ClusterBuilder(@config)
    @config = @preBuildBuilder.changeConfig(@config, @clusterBuilder.cluster) if @clusterBuilder.cluster

    console.log("XML Builder")
    @xmlBuilder = new XMLBuilder(@config)

    console.log("HTML Builder")
    @htmlBuilder = new HTMLBuilder(@config)

    console.log("Scripts Builder")
    @scriptsBuilder = new ScriptsBuilder(@config)

    console.log("Style Builder")
    @styleBuilder = new StyleBuilder(@config)

    console.log("Assets Builder")
    @assetsBuilder = new AssetsBuilder(@config)

    console.log("Common Builder")
    @commonBuilder = new CommonBuilder(@config)
    
    console.log("Build Cleaner")
    @buildCleaner = new BuildCleaner(@config)

    console.log("Config Build Build")
    @configBuildBuilder = new ConfigBuildBuilder(@config)

    console.log("Post Build Build")
    @postBuildBuilder = new PostBuildBuilder(@config)

    console.log('\n===================================\n\tCreate tasks\n===================================\n')
    @createGulpTasks()

#abstract method, expect to be overridden in child classes
  createGulpTasks: ->

module.exports = BasePlatform