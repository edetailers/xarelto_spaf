gulp = require 'gulp'
rename = require 'gulp-rename'
fse = require 'fs-extra'
$ = require("gulp-load-plugins")(lazy: true)

DEFAULT_THUMB_NAME = '200x150'
PRESENTATION_FILE = 'presentation.json'

BasePlatform = require './BasePlatform'

class CegedimPlatform extends BasePlatform

  createGulpTasks: =>
    pn = @config.platformname_short

    gulp.task pn+'-single', [pn+'-clean-dist'],           @single
    gulp.task pn+'-generate-params',                      @xmlBuilder.generateParameters
    gulp.task pn+'-add-params', [pn+'-generate-params'],  @addParametersXML
    gulp.task pn+'-clean-dist', [pn+'-copy-thumb'],       @commonBuilder.clean
    gulp.task pn+'-add-exports',[pn+'-add-params'],       @addExportFiles
    gulp.task pn+'-copy-thumb', [pn+'-add-exports'],      @addThumbnail

#---------------------------------------------------------------------------------------------------
# update ag build for cegedim
#---------------------------------------------------------------------------------------------------
  single:=>
    @buildToZip()

  addParametersXML:=>
    buildPathSingle = @config.dir.build_path_single
    parametersPath = @config.dir.platform_files.app_config.src + 'parameters/**'
    gulp.src parametersPath
      .pipe gulp.dest buildPathSingle + 'parameters/'
      .on 'end', ()->
        console.log "=========================="
        console.log "    Parameters copied     "
        console.log "=========================="

  addExportFiles: =>
    @pathSrc = @config.dir.platform_files.app_config.src
    buildPathSingle = @config.dir.build_path_single
    exportPath = @pathSrc + 'export/**'
    exportTemplatePath = @pathSrc + 'export.html'
    exportPublicPath = @pathSrc + 'public/**'

    gulp.src exportPath
      .pipe gulp.dest buildPathSingle + 'export'

    gulp.src exportTemplatePath
      .pipe gulp.dest buildPathSingle

    gulp.src exportPublicPath
      .pipe gulp.dest buildPathSingle + 'public'

      .on 'end', ()->
        console.log "=========================="
        console.log "    Export Files copied   "
        console.log "=========================="

  addThumbnail: =>
    thumbName = @_getClusterName() or DEFAULT_THUMB_NAME
    thumbnail = @config.dir.build_path_single + thumbName + '.jpg'
    thumbnailPath = @config.dir.build_path_single + @config.dir.thumbnail_simple

    gulp.src thumbnail
      .pipe rename basename: DEFAULT_THUMB_NAME
      .pipe gulp.dest thumbnailPath
      .on 'end', ()->
        console.log "=========================="
        console.log "      Thumbnail added "
        console.log "=========================="

  buildToZip:=>
    gulp.src ["**"] ,cwd:@config.dir.build_path_single
      .pipe $.zip  @config.dir.zip_name, cwd: @config.dir.build
      .pipe gulp.dest @config.dir.distribution
      .on 'end', =>
        console.log  "====== DEPLOYED for single " + @config.platformname + "=========="

  _getJSON: (path)->
    fse.readJsonSync(path)

  _getClusterName: ->
    presentationJSON = @_getJSON @config.dir.build_content_folder + '/' + PRESENTATION_FILE
    presentationJSON.storyboards[presentationJSON.storyboard[0]].cluster

module.exports = CegedimPlatform