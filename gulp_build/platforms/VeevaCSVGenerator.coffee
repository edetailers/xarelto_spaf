gulp = require "gulp"
fs = require "fs-extra"
jsoncsv = require "json-csv"

CSV_NAME  = "multichannelDataCreate.csv"
CSV_PDF_NAME = "multichannelPdfCreate.csv"
CSV_VIDEO_NAME = "multichannelVideoCreate.csv"

class Item
  constructor: (data)->
    @id = data.id
    @name = data.name
    @type = data.type
    @crmMediaType = data.crmMediaType
    @presentationLink = data.link.presentation_id
    @collectionId = data.link.collection_id
    @relatedResourceId = data.link.related_resource_id
    @keyMessagePrefix = @_getPrefix(data)
    @slideshow = @_getSlideshow(data)
    @fullName = data.fullName or @_getFullName(data)
    @customName = data.customName
    @mediaFileName = data.mediaFileName or @_getMediaFileName(data, @fullName)
    @documentId = data.documentId or @_getDocumentId(data)
    @disableActions = data.link.disableActions or ""
    @customReaction = data.link.customReaction or ""
    @isSlide = @type.toLocaleUpperCase() is "SLIDE"
    @distributionGroup = data.link.distributionGroup
    @businessUnit = data.link.businessUnit
    @indication = data.link.indication
    @isShared = data.isShared

  _getPrefix: (data)->
    data.link.presentation_prefix or data.slideshow

  _getSlideshow: (data)->
    data.slideshow

  _getFullName:(data)-> @keyMessagePrefix + "_" + data.id

  _getMediaFileName: (data, fullName)->
    if data.isAddPrefix
      fullName + data.fileType
    else
      data.id + data.fileType

  _getDocumentId: (data)->
    if data.link.document_start_slide__id and (typeof data.index is 'number')
      data.link.document_start_slide__id + data.index
    else ''

class VVCSVBuild
  constructor: (@config, @localConfig)->
    @countryField = "country"
    @productField = "product"
    @coutryCode = ""
    @productCode = ""
    @isShared    = false
    @getLocalData()
    @createGulpTasks()
    @getJsonData()
    @getAnalitycsData()

  getLocalData: ()=>
    @localData = fs.readJsonSync(@localConfig).veeva
    @product = @localData.product_name
    @country = @localData.country
    @language = @localData.language

  getJsonData: ()=>
    @json = fs.readJsonSync @config.dir.source + 'platforms/rainmaker/presentation.json'

  getAnalitycsData: ()=>
    @analitycsData  = @getAnalyticsData()

  getStoryboard: (collectionId)=> @json.storyboards[collectionId].content

  getStructure: (slideshowId)=> @json.structures[slideshowId].content

  checkArrayPresentations:(array, currentPresentation)->
    array.find ((item) -> item.presentation_id == currentPresentation.presentation_id)

  filterPresentationsByCode: (jsonData, value, field)->
    return jsonData unless value
    jsonData.filter (presentation)->
      presentation[field] is value

  createSharedKeyMessage: (mediaFileFormat, isShared)->
    item = @localData.shared_key_message
    options =
      Object.assign(mediaFileFormat, {
        id: item.id
        name: item.name
        index: ""
        fullName: item.name
        isShared: isShared
        link:
          presentation_name: ''
          related_resource_id: ''
          presentation_prefix: ''
        documentId: item.document_id
      })
    new Item(options)

  createItemsCollections: (mediaFileFormat, isUseCustomName)=>
    itemsArray = []
    filterPresentations = @filterPresentationsByCode(@filterPresentationsByCode(@localData.presentations, @countryCode, @countryField), @productCode, @productField)

    filterPresentations.forEach (presentation)=>
      itemsArray.push(presentation) if !@checkArrayPresentations(itemsArray, presentation)

      index = 0
      @getStoryboard(presentation.collection_id)
        .forEach (slideshow)=>
          itemsArray = itemsArray.concat(
            @getStructure(slideshow)
              .map (slideId)=>
                dataWithName = @json.slides[slideId].monitoring || @json.slides[slideId]
                slideAnalyticsData = @analitycsData[@getPresentationSlideKey({collectionId: presentation.collection_id, slideshow: slideshow, id: slideId})]
                {
                  id: slideId,
                  name: if isUseCustomName then slideAnalyticsData.description else dataWithName.name
                  customName: if isUseCustomName then slideAnalyticsData.id else null
                }
              .map (slideObj)->
                Object.assign({
                  index: index++,
                  link: presentation,
                  slideshow: slideshow
                }, mediaFileFormat, slideObj)
              .map (options)->
                new Item(options)
          )
    itemsArray

  getItemsByPath:(dirPath, mediaFileFormat, presentationConfig, isUseCustomName)->
    fs.readdirSync(dirPath)
      .filter (fileName)-> fileName.indexOf(mediaFileFormat.fileType) isnt -1
      .map @createPdfCsvItem.bind(@, mediaFileFormat, presentationConfig, isUseCustomName)

  createPdfCsvItem: (mediaFileFormat, presentationConfig, isUseCustomName, pdfName, index)->
    jsonData = if isUseCustomName then @analitycsData else null
    clearName = pdfName.replace(mediaFileFormat.fileType, "")
    options = Object.assign(mediaFileFormat, {
      id: clearName,
      name: if jsonData then jsonData[clearName].description else clearName
      customName: if isUseCustomName then jsonData[clearName].id else null
      index: index,
      link: presentationConfig
    })
    new Item (options)

  isCFEnv: ()->
    process.argv.indexOf("--cf") > -1

  isAnalyticsData: ()->
    process.argv.indexOf("--analytics") > -1

  getArgValue: (value, args)->
    args.find (item)-> item.indexOf(value) isnt -1
      ?.split('=')[1]

  createGulpTasks: =>
    gulp.task "vw-csv", ()=>
      argsList = process.argv
      @productCode = @getArgValue "--product", argsList
      @countryCode = @getArgValue "--country", argsList
      @isUseCustomName = @isAnalyticsData()
      @isShared    = argsList.indexOf("--shared") > -1
      items = @createItemsCollections(@config.mediaFileFormats.slide, @isUseCustomName)
      items.push @createSharedKeyMessage(@config.mediaFileFormats.shared, @isShared) if @isShared
      @generateCSV(items, CSV_NAME, @isCFEnv())

    gulp.task "vw-pdf-csv", ()=>
      flow = @getArgValue '--flow', process.argv
      @isUseCustomName = @isAnalyticsData()
      items = []
      if flow
        media = fs.readJsonSync(@config.dir.media)
        items = Object.keys(media)
          .reduce (accumulator, key) ->
            if media[key].flow.indexOf(flow) isnt -1 and key.indexOf('shared/pdfs') isnt -1
              splittedKey = key.split('/')
              pdfName = splittedKey[splittedKey.length - 1]
              accumulator.push(pdfName)
            accumulator
          , []
          .map @createPdfCsvItem.bind(@, @config.mediaFileFormats.pdf, @localData["pdfPresentation"], @isUseCustomName)
      else
        items = @getItemsByPath(@config.dir.pdfPath, @config.mediaFileFormats.pdf, @localData["pdfPresentation"], @isUseCustomName)
      items.unshift(@localData["pdfPresentation"])
      @generateCSV(items, CSV_PDF_NAME, @isCFEnv())

    gulp.task "vw-video-csv", ()=>
      @isUseCustomName = @isAnalyticsData()
      items = @getItemsByPath(@config.dir.videoPath, @config.mediaFileFormats.video, @localData["videoPresentation"], @isUseCustomName)
      items.unshift(@localData["videoPresentation"])
      @generateCSV(items, CSV_VIDEO_NAME, @isCFEnv(), @isAnalyticsData())

  getCSVFields: (isCFEnvironment)=>
    if (isCFEnvironment)
      [@config.multichannelCSV.fields..., @config.multichannelCSV.CFFields...]
    else
      @config.multichannelCSV.fields

  getAnalyticsData: ()=>
    fs.readJsonSync(@config.dir.analytics)

  getPresentationSlideKey: (item)=>
    item.collectionId + '/' + item.slideshow + '/' + item.id

  generateCSV: (itemsCollections, CSVName, isCFEnv)=>
    CONFIG_FIELDS = @getCSVFields(isCFEnv)

    createCsvFile = (err, csv)->
      fs.writeFile(CSVName, csv, {encoding: 'utf8'})

    createSlidesData = (itemsArray)=>
      itemsArray.map (item)=>
        obj = {}
        CONFIG_FIELDS.forEach (field)=>
          _value = ''
          switch field.name
            # common fields && slide fields
            when 'external_id__v'         then _value = field._get(item.presentation_id)
            when 'presentationLink'       then _value = field._get(item.presentationLink)
            when 'document_id__v'         then _value = field._get(item.document_id or item.documentId)
            when 'type'                   then _value = field._get(item.type)
            when 'lifecycle__v'           then _value = field._get(item.isSlide or item.isShared)
            when 'name__v'                then _value = field._get(item.customName or item.fullName or item.presentation_name)
            when 'slideTitle'             then _value = field._get(item.name)
            when 'slidefilename'          then _value = field._get(item.mediaFileName)
            when 'productName'            then _value = field._get(@product, item.isSlide or item.isShared)
            when 'slideCountry'           then _value = field._get(@country, item.isSlide or item.isShared)
            when 'presLanguage'           then _value = field._get(@language, item.isSlide or item.isShared)
            when 'slideLanguage'          then _value = field._get(@language, item.isSlide or item.isShared)
            when 'clmContent'             then _value = field._get(item.isSlide or item.isShared)
            when 'engageContent'          then _value = field._get(item.isSlide or item.isShared)
            when 'crmMediaType'           then _value = field._get(item.crmMediaType)
            when 'relatedSharedResource'  then _value = field._get(item.relatedResourceId, item.isSlide)
            when 'crm_SharedResource'     then _value = field._get('YES', item.isShared)
            when 'disableActions'         then _value = field._get(item.disableActions, item.isSlide or item.isShared)
            when 'customReaction'         then _value = field._get(item.customReaction, item.isSlide or item.isShared)
            when 'slideDistributionGroup' then _value = field._get(item.distributionGroup, item.isSlide )
            when 'slideBusinessUnit'      then _value = field._get(item.businessUnit, item.isSlide )
            when 'slideIndication'        then _value = field._get(item.indication, item.isSlide )
            # presentation fields
            when 'presentationId'         then _value = field._get(item.presentation_id)
            when 'hiddenId'               then _value = field._get(item.hidden)
            when 'productNamePres'        then _value = field._get(@product, item.isSlide or item.isShared)
            when 'countryName'            then _value = field._get(@country, item.isSlide or item.isShared)
            when 'content'                then _value = field._get('YES', item.isSlide or item.isShared)
            when 'engageContentPres'      then _value = field._get('NO', item.isSlide or item.isShared)
            when 'coBrowse'               then _value = field._get('NO', item.isSlide or item.isShared)
            when 'presDistributionGroup'  then _value = field._get(item.distributionGroup, item.isSlide)
            when 'presBusinessUnit'       then _value = field._get(item.businessUnit, item.isSlide)
            when 'presIndication'         then _value = field._get(item.indication, item.isSlide)
            else                         _value = field._get()
          obj[field.name] = _value
        obj
    multichannelSlidesData = createSlidesData itemsCollections
    jsoncsv.csvBuffered multichannelSlidesData, {fields: CONFIG_FIELDS}, createCsvFile

module.exports = VVCSVBuild
