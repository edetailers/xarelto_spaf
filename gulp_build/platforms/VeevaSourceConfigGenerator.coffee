gulp = require 'gulp'
fs = require 'fs-extra'
fileExists = require 'file-exists'

PDFS_CONFIG = 'pdfs-config.json'
VIDEOS_CONFIG = 'videos-config.json'

class VVSourceConfigBuild
  constructor: (@config, @localConfig)->
    @getLocalData()
    @generateGulpTasks()

  getArgValue: (args, key)->
    args?.find (item)-> item.indexOf(key) isnt -1
      ?.split('=')[1]

  getFlowPdfs: (args, key)->
    flow = @getArgValue(args, key)
    items = null
    if flow
      media = fs.readJsonSync(@config.dir.media)
      items = Object.keys(media)
        .reduce (accumulator, key) ->
          if media[key].flow.indexOf(flow) isnt -1 and key.indexOf('shared/pdfs') isnt -1
            splittedKey = key.split('/')
            pdfName = splittedKey[splittedKey.length - 1]
            accumulator.push(pdfName)
          accumulator
      , []
    items

  getLocalData: ()=>
    @localData = if fileExists @localConfig then fs.readJsonSync(@localConfig).veeva else {}
    @veevaId = @localData.veeva_id or ''
    @pdfPresentation = @localData.pdfPresentation or ''
    @videoPresentation = @localData.videoPresentation or ''

  generateGulpTasks: =>
    @createGulpTask('vw-pdf-config', {
      path: @config.dir.pdfPath
      dataFormat: @config.mediaFileFormats.pdf
      presentationConfig: @pdfPresentation
      config: PDFS_CONFIG
      flowFilteredSet: @getFlowPdfs(process.argv, '--flow')
    })
    @createGulpTask('vw-video-config', {
      path: @config.dir.videoPath
      dataFormat: @config.mediaFileFormats.video
      presentationConfig: @videoPresentation
      config: VIDEOS_CONFIG
    })

  createGulpTask: (name, options)=>
    gulp.task name, ()=>
      if (fs.existsSync(options.path))
        items = @getItemsByPath(options.path, options.dataFormat, options.presentationConfig.document_start_slide__id, options.flowFilteredSet)
        @generateFile(@config.dir.source + options.config, items, options.presentationConfig.presentation_id, options.path)
      else
        console.log(options.path + " doesn't exist")

  getItem: (name, index)->
    {
      key: name
      value: index
    }

  getItemsByPath:(dirPath, mediaFileFormat, startSlideId, flowFilteredSet)=>
    data = flowFilteredSet or fs.readdirSync(dirPath).filter (fileName)-> fileName.indexOf(mediaFileFormat.fileType) isnt -1
    data.map @clearNamesForVeeva.bind(@, mediaFileFormat, startSlideId)

  clearNamesForVeeva: (mediaFileFormat, startSlideId ,itemName, index)->
    clearName = itemName.replace(mediaFileFormat.fileType, '')
    clearId = if startSlideId then startSlideId + index else index
    @getItem(clearName, @veevaId + '_' + clearId)

  generateFile: (pathFile, data, presentationId, pathFiles)=>
    fileData = {
      presentation: presentationId
      source: pathFiles.replace(@config.dir.source, '') + '/'
      items: {}
    }
    data.forEach (item)-> fileData.items[item.key] = item.value
    fs.writeJsonSync pathFile, fileData

module.exports = VVSourceConfigBuild
