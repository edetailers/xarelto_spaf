gulp = require 'gulp'
fs = require 'fs-extra'
fileExists = require 'file-exists'

PRESENTATIONS_CONFIG = 'presentations-config.json'

class VVPresentationConfigBuild
  constructor: (@config, @localConfig)->
    @getLocalData()
    @generateGulpTasks()

  getLocalData: ()=>
    @localData = if fileExists @localConfig then fs.readJsonSync(@localConfig).veeva else {}
    @presentationsData = @localData.presentations or ''

  generateGulpTasks: =>
    @createGulpTask('vw-presentations-config', {
      presentationsConfig: @presentationsData
      config: PRESENTATIONS_CONFIG
    })

  createGulpTask: (name, options)=>
    gulp.task name, ()=>
      @generateFile(@config.dir.source + options.config, options.presentationsConfig)

  generateFile: (pathFile, data)=>
    fileData = {
      items: {}
    }
    data.forEach (item)-> fileData.items[item.collection_id] = item.presentation_id
    fs.writeJsonSync pathFile, fileData

module.exports = VVPresentationConfigBuild
