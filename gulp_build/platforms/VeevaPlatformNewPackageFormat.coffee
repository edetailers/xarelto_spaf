gulp = require 'gulp'
fs = require 'fs-extra'
fileExists = require 'file-exists'
del = require 'del'
vinylPaths = require 'vinyl-paths'
gulpIgnore = require 'gulp-ignore'
$ = require("gulp-load-plugins")(lazy: true)

BasePlatform = require './BasePlatform'

EXTERNAL_OPTION = '--wide-media'
PATH_MEDIA = /(shared\/)(pdfs|videos)\//g
REG_EXP_FILE_MEDIA = /(shared\\)(pdfs|videos)\\/g

class VeevaPlatformNewPackageFormat extends BasePlatform
  constructor: (@config)->
    @wm = @parse(EXTERNAL_OPTION)

  parse: (option)->
    parseValue = process.argv
      .filter (value)-> value if value.indexOf(option) > -1
      .map (value)-> value.split('=').pop()
      .find ()-> true
    parseValue or ''

  createGulpTasks: =>
    pn = @config.platformname_short

    gulp.task pn + '-single-new', [pn + '-remove-jsons'], @single
    gulp.task pn + '-get-new-configs', @getConfigs
    gulp.task pn + '-clean-new-dist', [pn + '-get-new-configs'], @commonBuilder.clean
    gulp.task pn + '-copy-new-dist', [pn + '-clean-new-dist'], @copyToDist
    gulp.task pn + '-set-configs', [pn + '-exclude-media'], @setVeevaConfigs
    gulp.task pn + '-exclude-media', [pn + '-copy-new-dist'], @excludeExternalMedia
    gulp.task pn + '-remove-images', [pn + '-set-configs'], @removeUnUsedImages
    gulp.task pn + '-remove-jsons', [pn + '-remove-images'], @removeUnnecessaryJson

  #  build for deep (single) slide-presentation
  single: =>
    @buildToZip()

  getConfigs: =>
    try
      @keyMessageConfigs = fs.readFileSync @config.dir.platform_files.app_config.configs, "utf-8"
      @keyMessageId = @config.dir.platform_files.app_config.key_message
      @data = JSON.parse @keyMessageConfigs
      @keyMessageName = @data[@keyMessageId]
    catch error
      console.log error

  getMediaConfig: =>
    return unless @wm
    configSource = {}
    configSource.pdfs = @getSourceConfigData @config.dir.pdf_configs
    configSource.videos = @getSourceConfigData @config.dir.video_configs
    '<script>setTimeout(function() {var configStrings = ' + JSON.stringify(configSource) + ';  if (app) app.config.set(\'configSource\', configStrings);}, 0);</script>'

  getSourceConfigData: (configPath)=>
    path = @config.dir.build_path_single + configPath
    if fileExists path then fs.readJsonSync path else {}

  copyToDist: =>
    gulp.src ["**"], cwd: @config.dir.build_content_folder
      .pipe gulp.dest @config.dir.distribution_build
      .on 'end', =>
        console.log "copy rainmaker build done"

  excludeExternalMedia: =>
    return unless @wm
    pdfsFolder = @config.dir.distribution_build + @config.dir.pdfs_path
    videosFolder = @config.dir.distribution_build + @config.dir.videos_path
    @excludedMediaList = @getExcludedMedia()
    gulp.src [pdfsFolder + '/*', videosFolder + '/*']
      .pipe(gulpIgnore.exclude(@isContain))
      .pipe(vinylPaths(del))
      .pipe gulp.dest @config.dir.distribution_media
      .on 'end', =>
        console.log "exclude external files"

  isContain: (file)=>
    name = file.path.split(REG_EXP_FILE_MEDIA).pop()
    @excludedMediaList.indexOf(name) is -1

  getExcludedMedia: =>
    try
      data = fs.readFileSync @config.dir.media, 'utf-8'
      Object.keys(JSON.parse data).map (item)->
        if item.match(PATH_MEDIA)
          item = item.replace(PATH_MEDIA, '')
        item
    catch error
      throw error

  setVeevaConfigs: =>
    indexHtml = @config.dir.distribution_build + 'index.html'
    customInjection = if @wm then 'app.isMediaWide = true;' else ''
    scriptString = '<script>setTimeout(function() {if (app) { app.isVeevaDeep = true;' + customInjection + '}}, 0);</script>'

    fs.readFile indexHtml, 'utf8', (err, data)=>
      if err
        console.error err
      else
        updated = data.replace(/<!--veevaDeepFlag-->/g, scriptString)
        updated = updated.replace(/<!--mediaConfigs-->/g, @getMediaConfig())
        fs.writeFile indexHtml, updated, (err)=>
          if err
            console.error err
          else
            console.log "index.html file was updated"

  removeUnUsedImages: =>
    del([@config.dir.distribution_build + "/*.jpg"])

  removeUnnecessaryJson: =>
    del([@config.dir.distribution_build + "/*-config.json", @config.dir.distribution_build + "/*-strings.json"])

  buildToZip: =>
    gulp.src ["**"], cwd: @config.dir.distribution_build, nodir: true
      .pipe $.zip @keyMessageName + '.zip', cwd: @config.dir.build_path_single
      .pipe gulp.dest @config.dir.dest
      .on 'end', =>
        console.log 'build to zip done'

module.exports = VeevaPlatformNewPackageFormat