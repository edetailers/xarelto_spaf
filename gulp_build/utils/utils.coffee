
module.exports =

  getFolders: (dir)->
    fs.readdirSync(dir).filter (file)->
      _path = path.join(dir, file)
      isDirectory = fs.statSync( _path ).isDirectory()
      isNotEmpty  = if isDirectory then fs.readdirSync( _path ).length isnt 0

      isDirectory and isNotEmpty

  loopOverSlides:(func) => @getFolders(@config.dir.slides_root).map func
