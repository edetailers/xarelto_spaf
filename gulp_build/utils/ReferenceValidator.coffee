fse         = require 'fs-extra'
gulp        = require 'gulp'

class ReferenceValidator

  constructor: (@config, referencesConfig)->
    moduleConfig = fse.readJsonSync(referencesConfig)
    Object.assign(@config, moduleConfig)

    gulp.task "rm-check-references", () =>
      @checkReferences()

  checkReferences: () ->
    @referencesList = @_getReferenceKeys()
    slidesDir = "#{@config.dir.dest}/#{@config.dir.slides_root}"
    slidesList = fse.readdirSync(slidesDir)

    Promise.all slidesList.map (slide) =>
      slidePath = "#{slidesDir}/#{slide}/#{slide}.html"
      @searchErrorReferences(slide, slidePath)

  searchErrorReferences: (slideName, slidePath) ->
    new Promise (resolve, reject) ->
      fse.readFile(slidePath, 'utf8', (err, html) ->
        unless err
          resolve html
        else
          reject slidePath
      )
    .then (slideHtml) =>
      @_validateRefsOnSlide(slideName, slideHtml)
    .catch (errorPath) ->
      console.log "\x1b[31m \tCan`t read #{errorPath} \x1b[0m"

  _validateRefsOnSlide: (slideName, html) ->
    slideRefKeys = @_getSlideRefKeys(html)
    if slideRefKeys.length
      errorKeys = @_getErrorKeys(slideRefKeys)
      if errorKeys.length
        console.log "\x1b[31m \tError in references! Slide: #{slideName}. Wrong key(s): #{errorKeys} \x1b[0m"

  _getErrorKeys: (slideRefList) ->
    errorKeys = []
    slideRefList.forEach (ref) =>
      ref.forEach (item) =>
        if @referencesList.indexOf(item) < 0
          errorKeys.push item
    errorKeys

  _getSlideRefKeys: (slidesHtml) ->
    attributes = @_parseAttributes(slidesHtml)
    @_parseKeys(attributes)

  _parseAttributes: (slidesHtml) =>
    attributesPattern = new RegExp("#{@config.references.dataAttr}\\s?=\\s?[\'\"]([^\'\"]+)[\'\"]", 'g')
    slidesHtml.match(attributesPattern) || []

  _parseKeys: (attributes) ->
    redundantChars = /(\\|"|'|=)/g
    separators = new RegExp("[#{@config.references.separator}#{@config.references.groupSeparator}]")
    attributes.map (attr) =>
      attr
        .replace(@config.references.dataAttr, '')
        .replace(redundantChars,'')
        .trim()
        .split(separators)

  _getReferenceKeys: () ->
    refsJson = @_getReferenceJson()
    keysList = Object.keys refsJson
    idProperty = 'referenceId'
    if refsJson[keysList[0]].hasOwnProperty idProperty
      keysList.map (key) -> refsJson[key][idProperty]
    else
      keysList

  _getReferenceJson: () =>
    refsJson = fse.readJsonSync(@config.dir.source + @config.references.file)
    refsJson.references || refsJson

module.exports = ReferenceValidator