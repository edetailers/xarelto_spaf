gulp = require 'gulp'
path = require 'path'
es = require 'event-stream'
changed = require 'gulp-changed' #pipe trough only changed files
plumber = require 'gulp-plumber'
coffeelint = require 'gulp-coffeelint'
eslint = require 'gulp-eslint'
gulpif = require 'gulp-if'
lazypipe = require 'lazypipe'
insert = require 'gulp-insert'
fse = require 'fs-extra'

BaseBuilder = require './BaseBuilder'

PATH_SEPARATOR = path.sep

coffeeChannel = (slideFolder) ->
  lazypipe()
  .pipe(insert.transform, (coffeeSource) -> processScriptsSource(coffeeSource, slideFolder))
  .pipe(coffeelint)
  .pipe(coffeelint.reporter)()

jsChannel = lazypipe()
  .pipe(eslint, {fix: true})
  .pipe(eslint.format)

processScriptsSource = (coffeeSource, slideFolder) ->
  if slideFolder
    injectStrings(coffeeSource, JSON.stringify(fse.readJsonSync(slideFolder + 'strings.json')))
  else
    coffeeSource

injectStrings = (coffeeSource, slideStrings) ->
  coffeeSource.replace '\'%=slideStrings%\'', slideStrings

class ScriptBuilder extends BaseBuilder
  constructor: (@config)->

  errorHandler: (err)->
    console.log(err)
    @emit('end')

  slidesScripts: ()=>
    console.log "\n======== Start Styles =======\n"

    processStreams = @loopOverSlides (folder)=> @oneSlideScripts(folder)

    mergedStream = es.merge.apply(@, processStreams)
    mergedStream.on 'end', ()->
      console.log "\n======== Compiled css/stylus =======\n"

  oneSlideScripts: (folder)=>
    slideFolder = @config.dir.source + @config.dir.slides_root + folder + PATH_SEPARATOR
    scriptsSource = slideFolder + '**/*.{js,coffee}'
    dest = @config.dir.dest + @config.dir.slides_root + folder

    console.log "Scripts for #{folder}"

    gulp.src(scriptsSource)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(gulpif(/^.*\.coffee$/, coffeeChannel(slideFolder)))
    .pipe(gulpif(/^.*\.js$/, jsChannel()))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))

  modulesScripts: =>
    source = @config.dir.source + @config.dir.modules_root + '**/*.{js,coffee}'
    dest = @config.dir.dest + @config.dir.modules_root

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(gulpif(/^.*\.coffee$/, coffeeChannel()))
    .pipe(gulpif(/^.*\.js$/, jsChannel()))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("modules scripts"))

  isFixed: (file) ->
    file.eslint && file.eslint != null && file.eslint.fixed

  fixJSLinter: (dest) ->
    source = dest + '**/*.js'
    gulp.src(source)
      .pipe(eslint({"fix": true}))
      .pipe(gulpif(@isFixed, gulp.dest(dest)))

  fixJS: =>
    @fixJSLinter(@config.dir.source + @config.dir.modules_root)
    @fixJSLinter(@config.dir.source + @config.dir.slides_root)

module.exports = ScriptBuilder
