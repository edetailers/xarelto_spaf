BaseBuilder = require './BaseBuilder'
rename      = require 'gulp-rename'
del         = require 'del'
gulp        = require 'gulp'

OPTIONS = {
  '--cb': '_buildChatbot'
}
class PostBuildBuilder extends BaseBuilder
  constructor: (@config)->

  build: ()=>
    buildSystem = @_detectOption()
    @[buildSystem]() if buildSystem

  _buildChatbot: ()->
    @_renameFiles()

  _detectOption: () ->
    buildProp = process.argv[process.argv.length - 1]
    OPTIONS[buildProp] or ''

  _renameFiles: ()->
    @config.dir.bot_configs.files.map (item)->
      gulp.src(item.path + item.file)
        .pipe(rename((file)->
          file.basename = item.newName
        ))
        .pipe(gulp.dest(item.path)).on 'end', ->
          del(item.path + item.file).then (paths) ->
            console.log '\nDeleted:\n\n', paths.join('\n')

module.exports = PostBuildBuilder