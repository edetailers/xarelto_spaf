xml = require 'xmlbuilder'
fse = require 'fs-extra'

BaseBuilder = require './BaseBuilder'

class XMLBuilder extends BaseBuilder
  constructor: (@config)->

  generateParameters: =>
    @_getPresentationJSON()
    @_writeParameters @_generateXMLData()

  _getPresentationJSON: ->
    @presentationJSON = @_getJSON @config.dir.build_content_folder + '/presentation.json'

  _getJSON: (path)->
    fse.readJsonSync(path)

  _getSlidesMap: ->
    slides = @presentationJSON.slides
    Object.keys(slides)
      .map (slide)->{'@pageid': slides[slide].id}

  _getSequenceId: ->
    configCegedimJSON = @_getJSON @config.dir.platform_files.app_config.src + 'config.json'
    cluster = @presentationJSON.storyboards[@presentationJSON.storyboard[0]].cluster
    clusterName = configCegedimJSON.sequenceSuffix[cluster]
    sequenceSuffix = if clusterName then '-' + clusterName else ''
    configCegedimJSON.sequencePrefix + sequenceSuffix

  _generateXMLData: ->
    xmlParams =
      'Sequence':
        '@Id': => @_getSequenceId()
        '@xmlns': 'urn:param-schema'
        'Pages':
          'Page': => @_getSlidesMap()

    xml.create(xmlParams).end({pretty: true, indent: '\t'})

  _writeParameters: (output)->
    destPath = @config.dir.platform_files.app_config.src + 'parameters/parameters.xml'
    fse.writeFileSync(destPath, output)

module.exports = XMLBuilder