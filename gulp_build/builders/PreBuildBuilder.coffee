del = require 'del'
fs = require 'fs-extra'
fileExists = require 'file-exists'
BaseBuilder = require './BaseBuilder'

REPLACE_PATH = '../shared/'
MODULES_IMG_REGEXP = /assets\//g
VW_OPTION = '--vw'
VW_STANDALONE_FLAG = 'sa'

class PreBuildBuilder extends BaseBuilder
  constructor: (@config)->
    @vw = @parse(VW_OPTION)

  parse: (option)->
    parseValue = process.argv
      .filter (value)-> value if value.indexOf(option) > -1
      .map (value)-> value.split('=').pop()
      .find ()-> true
    parseValue or ''

  migrate: ()->
    fs.copySync './app/', @config.dir.tmp_source
    console.log 'migrate'

  migrateSlides: ()->
    fs.copySync './app/slides/', @config.dir.tmp_source + 'slides/'
    fs.copySync './app/pug_fragments/', @config.dir.tmp_source + 'pug_fragments/'
    fs.copySync './app/global_styles/', @config.dir.tmp_source + 'global_styles/'
    fs.copySync './app/global-strings.json', @config.dir.tmp_source + 'global-strings.json'
    console.log 'migrate'

  renameModulesFolder: ()->
    #migrate after config update
    fs.moveSync @config.dir.tmp_source + 'modules/rainmaker_modules/', @config.dir.tmp_source + @config.dir.veeva_modules_root
    console.log 'rename folder rainmaker_modules to veeva_modules'

  changeConfig: (config, isCluster)->
    return config unless config.dir.tmp_source
    config.dir.source = config.dir.tmp_source
    config.dir.modules_root = config.dir.veeva_modules_root unless isCluster
    config.dir.localizer_module = 'app/' + config.dir.modules_root + 'ah-localizer'

    config.dir.json_sources = [
      config.dir.source + 'platforms/rainmaker/*.json',
      config.dir.source + '**/*.json',
      '!' + config.dir.source + 'slides/**/',
      '!' + config.dir.source + '_vendor/',
      '!' + config.dir.source + 'accelerator/',
      '!' + config.dir.source + 'platforms/agnitio/',
      '!' + config.dir.source + config.dir.platform_cegedim,
      '!' + config.dir.source + 'platforms/veeva/'
      ]
    config

  _getJSON: (path)->
    console.log path
    fs.readJsonSync(path)

  _readFile: (path, cb)->
    fs.readFile(path, 'utf8', cb)

  _getPresentation: ()->
    @presentationJSON = @_getJSON @config.dir.source + @config.dir.platform + 'presentation.json'

  _getIndexPath: ()->
    @config.dir.source + @config.dir.platform + 'index.pug'

  replacePathsInIndex: (index)->
    try
      @_readFile index, (err, template)=>
        if template.match @config.dir.rainmaker_template
          updated = template.replace(@config.dir.rainmaker_template, 'veeva_modules')
          @_writeFile index, updated, (err)->
            console.error err if err
    catch error
      throw error

  _updatePresentationModulesPath: (modulesJSON)->
    @presentationJSON.modules = JSON.parse(JSON.stringify(modulesJSON).replace(@config.dir.rainmaker_template, 'veeva_modules'))

  updateConfig: ()->
    data = @_rewriteConfigPaths @_getJSON(@config.dir.source + @config.dir.platform + 'config.json')
    data.configSource = {}
    data.configSource.pdfs = @getData(@config.dir.pdf_configs)
    data.presentationsConfig = @getData(@config.dir.presentations_config)
    @_writeJSON @config.dir.tmp_source + @config.dir.platform + 'config.json', data

  _rewriteConfigPaths: (jsonData)->
    dependenciesForRemove = @config.dir.exclude.tmp.concat @config.dir.exclude.ignore
    filterDependencies = jsonData.dependencies
    .filter (item)->
      item if dependenciesForRemove.indexOf(item.src) is -1
    .map (item)=>
      if @vw isnt VW_STANDALONE_FLAG
        item.src = REPLACE_PATH + item.src if item.src.indexOf(REPLACE_PATH) is -1
      item
    jsonData.dependencies = filterDependencies
    jsonData.monitoringAPI = ""
    jsonData

  _writeJSON: (path, data, cb)->
    fs.writeJson(path, data, cb)
    console.log(path + ' updated')

  _writeFile: (path, data, cb)->
    fs.outputFile path, data, 'utf8', cb

  delete: (path, callback)->
    del(path)
    .then ()->
      if callback
        console.log JSON.stringify callback
        callback()

      console.log 'delete ' + path

  _getSlideshows: ()->
    Object.keys(@presentationJSON.storyboards).map (collection)=> @presentationJSON.storyboards[collection].content
      .reduce (prev, next)-> prev.concat next

  clearPlatforms: ()->
    @delete(@config.dir.tmp_source + @config.dir.platform_cegedim)

  clearTests: ()->
    @delete(@config.dir.tmp_source + @config.dir.test)

  clearModules: ()->
    paths = Object.keys(@presentationJSON.modules).filter (key)=>
      @presentationJSON.modules[key].ignoreVeeva
    .map (key)=>
      delete @presentationJSON.modules[key]
      @config.dir.tmp_source + @config.dir.modules_root + key

    @delete(paths, @updatePresentationJSON.bind(@))

  clearCustomFiles: ()->
    root = @config.dir.tmp_source + @config.dir.modules_root
    paths = @updatedDependencies(@config.dir.exclude.modules, root).concat @updatedDependencies(@config.dir.exclude.tmp, @config.dir.tmp_source)
    @delete(paths)

  clearSlides: ()->
    @delete(@config.dir.tmp_source + @config.dir.slides_root + '**/*')

  updatedDependencies: (array, prefix)->
    array.map (item)-> prefix + item

  updatePresentationJSON: ()->
    @_writeJSON  @config.dir.tmp_source + @config.dir.platform + 'presentation.json', @presentationJSON

  updateModulesImagesPaths: ()->
    Object.keys(@presentationJSON.modules).map (key)=>
      pathsToScript = @presentationJSON.modules[key].files.scripts
      pathsToScript?.map (path)=>
        localPath = @config.dir.tmp_source + path
        try
          @_readFile localPath, (err, script)=>
            if script && script.match MODULES_IMG_REGEXP
              updated = script.replace(MODULES_IMG_REGEXP, REPLACE_PATH + @config.dir.modules_root + key + '/assets/')
              console.log 'Replace images paths', key
              @_writeFile localPath, updated, (err)->
                console.error err if err
        catch error
          throw error


  build: ()=>
    console.log('start migration.')
    @migrate()
    @renameModulesFolder()
    @replacePathsInIndex(@_getIndexPath())
    @_getPresentation()
    @_updatePresentationModulesPath @presentationJSON.modules
    @updateConfig()
    @clearPlatforms()
    @clearTests()
    @clearModules()
    @updateModulesImagesPaths()
    @clearCustomFiles()
    @clearSlides()
    console.log('Build finished')

  buildSlides: ()=>
    @migrateSlides()
    @presentationJSON = @_getPresentation()
    @uniqueSlides = Object.keys(@presentationJSON.slides)
#    console.log @uniqueSlides

  clear: (slides)->
    paths = Object.keys(@presentationJSON.slides).filter (slide)-> slides.indexOf(slide) is -1
      .map (slide)=> @config.dir.tmp_source + @config.dir.slides_root + slide

    del(paths).then ()->
      console.log('Deleted files and folders')

  cleaner: =>
    @delete(@config.dir.tmp_source)

  getData: (path)-> if fileExists path then fs.readJsonSync(path) else {}

module.exports = PreBuildBuilder
