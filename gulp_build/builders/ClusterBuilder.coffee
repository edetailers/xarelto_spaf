del = require 'del'
fs = require 'fs-extra'

BaseBuilder = require './BaseBuilder'

CLUSTER_OPTION = '--cluster='
CLUSTER_FIELD = 'cluster'
PRESENTATION_FILE = 'presentation.json'
CONFIG_FILE = 'config.json'

class ClusterBuilder extends BaseBuilder
  constructor: (@config)->
    @cluster = @_getTargetCluster(CLUSTER_OPTION)

  _getTargetCluster: (option)->
    clusterArg = process.argv.find (value)-> value.indexOf(option) isnt -1
    clusterArg?.split(option).pop() or ''

  _writeJSON: (path, data)->
    fs.writeJson(path, data)

  _getJSON: (path)->
    fs.readJsonSync(path)

  _migrateToTmp: ()->
    fs.copySync './app/', @config.dir.tmp_source

  _getSlideshowsByCluster: () ->
    collections = @presentationJSON.storyboards
    Object.keys(collections)
      .filter (key)=> collections[key][CLUSTER_FIELD]?.indexOf(@cluster) >= 0
      .map (key)-> collections[key].content
      .reduce (prev, next)-> prev.concat next

  _getClusterSlides: (slideshows)->
    slideshows
      .map (slideshow)=> @presentationJSON.structures[slideshow].content
      .reduce (prev, next)-> prev.concat next

  _getClusterPopups: ()->
    Object.keys(@presentationJSON.slides)
      .filter (slide)=> @presentationJSON.slides[slide][CLUSTER_FIELD]?.indexOf(@cluster) >= 0

  _getAllClusterSlides: ()->
    clusterSlideshows = @_getSlideshowsByCluster()
    clusterSlides = @_getClusterSlides clusterSlideshows
    clusterPopups = @_getClusterPopups() or []
    [clusterSlides..., clusterPopups...]

  _getSlideshows: ()->
    Object.keys(@presentationJSON.storyboards)
      .map (collection)=> @presentationJSON.storyboards[collection].content
      .reduce (prev, next)-> prev.concat next

  _clearSlides: ()->
    Object.keys(@presentationJSON.slides)
      .filter (slide)=> !@clusterSlides.includes(slide)
      .forEach (slide)=> delete @presentationJSON.slides[slide]

  _clearCollections: ()->
    Object.keys(@presentationJSON.storyboards)
      .filter (collection)=>
        cluster = @presentationJSON.storyboards[collection][CLUSTER_FIELD]
        !cluster or cluster.indexOf(@cluster) is -1
      .forEach (collection)=> delete @presentationJSON.storyboards[collection]

  _isContainClusterSlides:(slideshow)->
    @presentationJSON.structures[slideshow].content
      .every (slide)=> @clusterSlides.includes(slide)

  _clearSlideshow: ()->
    slideshows = @_getSlideshows()
    Object.keys(@presentationJSON.structures)
      .filter (slideshow)=> !@_isContainClusterSlides(slideshow) && !slideshows.includes(slideshow)
      .forEach (slideshow)=> delete @presentationJSON.structures[slideshow]

  _updateSB: ()->
    @presentationJSON.storyboard[0] = Object.keys(@presentationJSON.storyboards)
      .filter (collection)=> @presentationJSON.storyboards[collection].start

  _generateStartPath: ()->
    collection = @presentationJSON.storyboard[0]
    slideshow = @presentationJSON.storyboards[collection].content[0]
    slide = @presentationJSON.structures[slideshow].content[0]
    collection + '/' + slideshow + '/' + slide

  _updatePlatformConfigJson: (path)->
    config = @_getJSON path + CONFIG_FILE
    config.startPath = @_generateStartPath()
    @_writeJSON path + CONFIG_FILE, config

  _clearPresentationStructure: ()->
    @_clearSlides()
    @_clearCollections()
    @_clearSlideshow()
    @_updateSB()

  _updateStructure: ()=>
    presentationJsonDir = @config.dir.tmp_source + @config.dir.platform
    presentationJsonPath = @config.dir.tmp_source + @config.dir.platform + PRESENTATION_FILE
    @presentationJSON = @_getJSON presentationJsonPath
    @clusterSlides = @_getAllClusterSlides()
    @_clearNotClusterSlides()
    @_clearPresentationStructure()
    @_writeJSON presentationJsonPath, @presentationJSON
    @_updatePlatformConfigJson presentationJsonDir

  clusterBuild: ()=>
    console.log('Start migration to tmp folder...')
    @_migrateToTmp()
    console.log('Start cluster build...')
    @_updateStructure()
    console.log('Cluster build finished!')

  _clearNotClusterSlides: ()->
    paths = Object.keys(@presentationJSON.slides)
      .filter (slide)=> !@clusterSlides.includes(slide)
      .map (slide)=> @config.dir.tmp_source + @config.dir.slides_root + slide
    del(paths).then ()->
      console.log('Deleted not cluster files and folders')

module.exports = ClusterBuilder