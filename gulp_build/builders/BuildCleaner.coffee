gulp = require 'gulp'
del = require 'del'
exec = require('child_process').exec
uglify = require 'gulp-uglify'
cleanCSS = require 'gulp-clean-css'
fse = require 'fs-extra'
jsonmin = require 'gulp-jsonmin'
coffee = require 'gulp-coffee'
stylus = require 'gulp-stylus'

CLM_OPTIONS = {
  '--ag': 'agnitio',
  '--vd': 'veeva_deep',
  '--vw': 'veeva_wide',
  '--cg': 'cegedim'
}

BaseBuilder = require './BaseBuilder'

class BuildCleaner extends BaseBuilder
  constructor: (@config)->

  errorHandler: (err)->
    console.log(err)
    @emit('end')

  build: ()=>
    destFiles = @config.dir.dest

    agnitioRainmakerProcces = exec('agnitio build',{cwd: destFiles, watch: false})
    agnitioRainmakerProcces.stdout.on 'data', (data)->
      console.log data

  cleaner: ()=>
    destFiles = @config.dir.dest
    @_clearModules()
    @_cleanJS destFiles
    @_cleanCSS destFiles

    paths = [
      destFiles + 'accelerator/css',
      destFiles + 'accelerator/js',
      destFiles + 'accelerator/CHANGELOG.md',
      destFiles + 'global_styles/*',
      '!' + destFiles + 'global_styles/assets',
      destFiles + 'slides/**/*.{coffee,styl}',
      destFiles + @config.dir.global_styles + '*.styl',
      destFiles + 'modules/**/*.{coffee,styl,js,css}',
      destFiles + 'modules/**/{model,package}.json'
    ]
    del(paths).then (paths)->
      console.log 'Deleted files and folders:\n', paths.join '\n'

  prodBuildCleaner: ()=>
    destFiles = @config.dir.dest
    path = destFiles + 'slides/**/*.{js,css}'

    Promise.all([
      @cleaner(),
      del(path).then (paths)->
        console.log 'Deleted files and folders:\n', paths.join('\n')
    ]).catch (error)->
      console.error error.message

  _cleanCSS: (destFiles)->
    gulp.src(destFiles+'build/presentation/*.css')
    .pipe(cleanCSS())
    .pipe(gulp.dest(destFiles+'build/presentation/'))

  _cleanJS: (destFiles)->
    gulp.src(destFiles+'build/presentation/*.js')
    .pipe(uglify())
    .pipe(gulp.dest(destFiles+'build/presentation/'))

  _clearModules: ()->
    @presentationJSONPath = @config.dir.dest + @config.dir.presentation_config
    @presentationJSON = fse.readJsonSync(@presentationJSONPath)
    @clmSystem = @_detectClm()
    @ignoredModules = @_getIgnoredModules()

    if @ignoredModules.length
      @_deleteModules()
      @_deleteFromPresentationJSON()

  _deleteModules: ()->
    paths = @ignoredModules.map (module)=>
      @config.dir.dest + @config.dir.modules_root + module
    del(paths)

  _deleteFromPresentationJSON: ()->
    @ignoredModules.forEach (module)=>
      delete @presentationJSON.modules[module]
    fse.writeJsonSync(@presentationJSONPath, @presentationJSON)

  _getIgnoredModules: ()->
    Object.keys(@presentationJSON.modules).filter (module)=>
      @presentationJSON.modules[module].ignoreClm and @presentationJSON.modules[module].ignoreClm.includes(@clmSystem)

  _detectClm: () ->
    buildProp = process.argv[process.argv.length - 1]
    CLM_OPTIONS[buildProp] or ''

  prettify: ()=>
    source = @config.dir.source
    gulp.src(source+'modules/**/*.styl')
      .pipe(stylus({
        compress: true
        'include css': true
      }))
      .pipe(gulp.dest(source+'modules/'))

  prettify1: ()=>
    path = [@config.dir.source + @config.dir.modules_root + '**/*.coffee', '!' + @config.dir.source + @config.dir.modules_root + '**/ah-dynamic-tracks.coffee']
    gulp.src(path)
      .pipe(coffee())
      .pipe(gulp.dest((file) -> file.base))

  prettify2: ()=>
    source = @config.dir.source

    gulp.src(source+'**/*.css')
      .pipe(cleanCSS({rebase: false}))
      .pipe(gulp.dest(source))

    del([source+'modules/**/*.{styl,coffee}', '!' + @config.dir.source + @config.dir.modules_root + '**/ah-dynamic-tracks.coffee'])

  prettify3: ()=>
    source = @config.dir.source

    gulp.src(source+'**/*.json')
      .pipe(jsonmin())
      .pipe(gulp.dest(source))

    gulp.src([source+'**/*.js', '!'+source+'**/ap-*.js'])
      .pipe(uglify())
      .pipe(gulp.dest(source))

module.exports = BuildCleaner