BaseBuilder = require './BaseBuilder'
fs          = require 'fs-extra'

class ConfigBuildBuilder extends BaseBuilder
  constructor: (@config)->

  build: ()=>
    @_setChatbotConfigs()

  _setChatbotConfigs: =>
    configPath = @config.dir.dest + @config.dir.bot_configs.config
    fs.readFile(configPath, (err, data)=>
      json = JSON.parse(data)
      json.isChatbot = true
      fs.writeFile(configPath, JSON.stringify(json)))

module.exports = ConfigBuildBuilder