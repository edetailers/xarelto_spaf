gulp        =  require 'gulp'
rename      = require 'gulp-rename'
del         = require 'del'
plumber     = require 'gulp-plumber'
changed     = require 'gulp-changed' #pipe trough only changed files
exec        = require('child_process').exec
path        = require 'path'
es          = require 'event-stream'
fse         = require 'fs-extra'
fileExists  = require 'file-exists'
filterFiles = require 'gulp-tap'
concat      = require 'gulp-concat'
font2css    = require('gulp-font2css').default
mv          = require 'mv'

imagemin = require 'gulp-imagemin'
pngquant = require 'imagemin-pngquant'
jpegoptim = require 'imagemin-jpegoptim'

jsonlint = require("gulp-jsonlint") # Validation JSON

BaseBuilder = require './BaseBuilder'

EXTENSIONS = '.{png,jpg,jpeg}'
IMAGES_MASK = '*' + EXTENSIONS
FONTS_MASK = '*.{woff,ttf,otf,svg,eot,woff2,ico}'
CONVERTED_FONTS_FILE = "typography.css"
PATH_SEPARATOR = path.sep
THUMBNAILS_NAMES =
  agnitio: 'thumbnail-agnitio'
REQUIRED_MODULES = [
  'ag-auto-menu',
  'ah-cleanup',
  'ag-overlay',
  'ah-localizer-helper',
  'ah-binding-handler',
  'ah-correct-events',
  'ah-history',
  'ah-utils',
  'ag-viewer',
  'ah-listener'
]

duplicatedImages = {}

class CommonBuilder extends BaseBuilder

  constructor: (@config)->

  errorHandler: (err)->
    console.log(err)
    @emit('end')

  changeAllJadetoPug: ->
    console.log '\nCreated:\n'
    gulp.src([ '**/*.jade' ]).pipe(rename((path) ->
      path.extname = '.pug'
      console.log path.dirname + '/' + path.basename + path.extname + '\n'
    )).pipe(gulp.dest('./')).on 'end', ->
      del([ '**/*.jade' ]).then (paths) ->
        console.log '\nDeleted:\n\n', paths.join('\n')

  renameJadeFragmentsFolder: =>
    @folder_jade = 'app/jade_fragments'
    @folder_pug = 'app/pug_fragments'
    fse.rename @folder_jade, @folder_pug, (err) ->
      if err
        throw err
    console.log "Renaming jade_fragments to pug_fragments done"

  copyJSONS: =>
    source = @config.dir.json_sources
    dest = @config.dir.dest

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
      .pipe(jsonlint())
      .pipe(jsonlint.reporter( (file)->
        console.error("File #{file.path} is not valid JSON.")
      ))
      .pipe(jsonlint.failOnError())
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("copy json data ready"))

  copyShared: =>
    source = @config.dir.source + @config.dir.shared + '**'
    dest = @config.dir.dest + @config.dir.shared

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("copy shared data ready"))

  copyIcons: =>
    dest = @config.dir.dest
    icons = @config.dir.source + @config.dir.shared + @config.dir.icons + IMAGES_MASK

    gulp.src(icons)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log('copy icons '))

  copyVendor: =>
    source = @config.dir.source + @config.dir.vendor + "**/*.*"
    dest = @config.dir.dest + @config.dir.vendor

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("copy vendor ready"))

  copyAccelerator: =>
    source = @config.dir.source + @config.dir.accelerator + "**/*.*"
    dest = @config.dir.dest + @config.dir.accelerator

    gulp.src(source)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(gulp.dest(dest))
    .on('end', ->
      console.log("copy accelerator ready"))

  slidesThumbnails: ()=>
    console.log "\n======== Start Thumbnails =======\n"

    processStreams = @loopOverSlides (folder)=> @oneSlideThumbnails(folder)

    mergedStream = es.merge.apply(@, processStreams)
    mergedStream.on 'end', ()->
      console.log "\n======== Compiled Thumbnails =======\n"

  imgOptimization:  =>
    source = @config.dir.source + "**/**/" + IMAGES_MASK
    dest = @config.dir.source

    gulp.src(source)
      .pipe(imagemin([
        pngquant(),
        jpegoptim({ progressive: true, max: 70})
      ],{
        verbose: true
      }))
      .pipe(gulp.dest(dest))


  getCorrectPath: (file)->
    file.path.split('\\').join('/')

  setDuplicated: (duplicatedImages, uniqueImages, imageName, file)=>
    if !duplicatedImages.hasOwnProperty(imageName)
      duplicatedImages[imageName] = []
      duplicatedImages[imageName].push(uniqueImages[imageName].path)
    duplicatedImages[imageName].push(@getCorrectPath(file))

  setUnique: (uniqueImages, imageName, file)=>
    uniqueImages[imageName] =
      size: fse.statSync(file.path).size
      path: @getCorrectPath(file)

  findImagesDuplicate: =>
    source = @config.dir.dest + '**/**/' + IMAGES_MASK
    uniqueImages = {}
    currentImage = {}
    gulp.src(source)
      .pipe filterFiles (file)=>
        imageName = path.basename(file.path)
        currentImage[imageName] = fse.statSync(file.path).size
        if uniqueImages.hasOwnProperty(imageName) && uniqueImages[imageName].size is currentImage[imageName] then @setDuplicated(duplicatedImages, uniqueImages, imageName, file) else @setUnique(uniqueImages, imageName, file)
        currentImage = {}
      .on('end', ->
        if Object.getOwnPropertyNames(duplicatedImages).length isnt 0 then console.log('duplicated images detected') else console.log('no duplicated images'))

  getExtension: (fileType, htmlExt, cssExt)-> if fileType is 'html' then htmlExt else cssExt

  getFilePath:(destination, objectName, fileType)=>
    root = if destination.indexOf(@config.dir.modules_root) isnt -1 then @config.dir.modules_root else @config.dir.slides_root
    htmlExt = '.html'
    cssExt = '.css'
    @config.dir.dest + root + objectName + path.sep + objectName + @getExtension(fileType, htmlExt, cssExt)

  getImgSrc:(destination, file, isGlobal)=>
    fileExtension = path.extname(path.basename file)
    foldersWayBack= if fileExtension is '.html' then '././' else '../../'
    imgFolder = if destination.indexOf(@config.dir.modules_root) isnt -1 then '' else @config.dir.images
    imgName = destination.replace(/^.*[\\\/]/, '')
    if isGlobal then src = foldersWayBack + @config.dir.shared + @config.dir.images + imgName else src = @config.dir.assets + imgFolder + imgName

  changePath:(destination, files, objectName)->
    isPathChanged = false

    files.forEach (file) =>
      editedFile = @replaceLocalPath(destination, file)
      if editedFile
        isPathChanged = true
        fse.writeFileSync(file, editedFile)
        console.log('img for ' + objectName + ' changed by source:' + file)

    if not isPathChanged then console.error('path for: ' + objectName + ' wasnt changed ' + destination)

  getFiles:(destination, objectName)=>
    [@getFilePath(destination, objectName, 'css'), @getFilePath(destination, objectName, 'html')]

  replaceLocalPath: (destination, file)=>
    sharedSrc = @getImgSrc(destination, file, true)
    localSrc = @getImgSrc(destination, file, false)
    localSrcRegex = new RegExp(localSrc, 'g')
    content = fse.readFileSync(file, 'utf8')
    editedContent = content.replace(localSrcRegex, sharedSrc)
    if editedContent is content then '' else editedContent

  copyImgToShared:(shared, sources)=>
    console.log('copy img to shared')
    imgName = sources[0].replace(/^.*[\\\/]/, '')
    sharedSrc = @config.dir.dest + shared + imgName
    if sources[0].indexOf(@config.dir.assets) isnt -1 then mv(sources[0], sharedSrc, (err)-> if err then console.log(err))

  getObjectName:(path)=>
    root = if path.indexOf(@config.dir.modules_root) isnt -1 then @config.dir.modules_root else @config.dir.slides_root
    regex = root + '(.*)' + '/' + @config.dir.assets
    filter = new RegExp(regex)
    path.match(filter)[1]

  changeImgDestination:(sources)=>
    sources.forEach (source) =>
      if source.indexOf(@config.dir.assets) isnt -1
        console.log('deleted file path= ' + source)
        objectName = @getObjectName(source)
        files = @getFiles(source, objectName)
        @changePath(source, files, objectName)
        del(source)
        console.log('=====================================')

  checkImgDestination:(sources)=>
    shared = @config.dir.shared + @config.dir.images
    if not @isSharedImg(sources) then @copyImgToShared(shared, sources)
    @changeImgDestination(sources)

  isSharedImg: (sources) =>
    for source in sources
      return true if source.indexOf(@config.dir.shared) isnt -1
    false

  fixImagesDuplicate: =>
    for img, source of duplicatedImages
      @checkImgDestination(source)

  oneSlideThumbnails: (folder)=>
    slideFolder = @config.dir.source + @config.dir.slides_root + folder + PATH_SEPARATOR
    thumbnailSource = slideFolder + PATH_SEPARATOR + 'thumbnails' + PATH_SEPARATOR + THUMBNAILS_NAMES.agnitio + EXTENSIONS

    dest = @config.dir.dest + @config.dir.slides_root + folder

    console.log "\tThumbnails for #{folder}"

    gulp.src(thumbnailSource)
    .pipe(plumber(handleError: @errorHandler))
    .pipe(changed(dest))
    .pipe(rename((file)->
      file.basename = folder
    ))
    .pipe(gulp.dest(dest))

  agnitioRun: =>
    agnitioRainmakerProcces = exec('agnitio run', {cwd: @config.dir.dest})
    agnitioRainmakerProcces.stdout.on 'data', (data)->
      console.log data
    agnitioRainmakerProcces.on 'exit', (code)->
      console.log "\x1b[31m'agnitio run dev' closed with code #{code}\x1b[0m"
    agnitioRainmakerProcces.on 'error', (error)->
      console.log "error"
    killProcess = ->
      agnitioRainmakerProcces.kill()

    process.on 'close', killProcess
    process.on 'uncaughtException', killProcess
    process.on 'error', killProcess
    process.on 'exit', killProcess

  clean: =>
    del(@config.dir.dest)

  getNumbersFormatter: =>
    localizerFolder = @config.dir.localizer_module
    return unless fileExists localizerFolder+"/cldr.json"
    data = fse.readJsonSync(@config.dir.configs, {throws: false})
    localeKey = data.locale
    dataForJson =
      "main": require("cldr-data/main/#{localeKey}/numbers.json").main
      "supplemental": require("cldr-data/supplemental/likelySubtags.json").supplemental

    fse.writeJsonSync(localizerFolder+"/cldr.json", dataForJson)
    console.log("------------------------------------")
    console.log("generate formatter for localization")
    console.log("------------------------------------")

  convertFonts: () =>
    srcPath = path.join @config.dir.source, @config.dir.global_styles, "assets/**/#{FONTS_MASK}"
    destPath = path.join @config.dir.dest, @config.dir.global_styles

    console.log "convert fonts to BASE64 and inject it in CSS"
    gulp.src srcPath
      .pipe font2css()
      .pipe concat(CONVERTED_FONTS_FILE)
      .pipe gulp.dest(destPath)

  checkModules: () =>
    _modulesRoot = @config.dir.source + @config.dir.modules_root
    _modules = @getFolders(_modulesRoot)
    missingModules = REQUIRED_MODULES.filter( (module) ->
      module unless module in _modules
    )
    unless missingModules.length
    then console.log 'all necessary modules are present'
    else console.log 'please install following modules: ' + missingModules.join(', ')


module.exports = CommonBuilder


