### Version 1.28.2
### 2018-10-11 - Vlad Kodenko
- Updating iscroll version to 5.2.0

### Version 1.28.1
### 2018-10-11 - Vlad Kodenko
- Addition of an ability to generate a video config and csv from shared/videos.

### Version 1.28.0
### 2018-10-01 - Anastasiia Zhdaniuk
- Add functionality for scale in veeva engaged. (Scale file, veeva-engaged class and styles).

### Version 1.27.0
### 2018-08-03 Andrii Romanyshyn
-  Fix issues with rm-fix-duplicate script and add it to build.

### Version 1.26.2
### 2018-09-26 - Nina Koval
- Add tmp folder to esliner's ignore files list

### Version 1.26.1
### 2018-08-17 - Anastasiia Zhdaniuk
- Fix issues for pre commit hook in VeevaCSVGenerator.coffee file.

### Version 1.26.0
### 2018-05-25 - Roman Kovalenko
-  Add ClusterBuilder, update build for cegedim: add posibility to build/upload by cluster.

### Version 1.25.3
### 2018-07-03 Kodenko Vlad
-  Add lintignor for root in general styles.

### Version 1.25.2
### 2018-06-27 - Nina Koval
-  Fix issue with monitoring on the first and on the last slide in veeva wide presentation (were added additional conditions with using methods from `ah-get-veeva-wide-order` module)

### Version 1.25.1
### 2018-06-08 - Nina Koval
-  Update PreBuildBuilder.coffee - was added replacing `rainmaker_modules` in index.pug

### Version 1.25.0
### 2018-06-28 Andrii Romanyshyn
-  Updated functionality for ignoring modules in different clms.

### Version 1.24.3
### 2018-06-26 - Oleksandr Melnyk
- Using monitoring.name property of slide(if exists) as slide name

### Version 1.24.2
### 2018-06-08 Vlad Kodenko
-  Added usage of css variables to the template. Separated the 'general-style.styl' into the 'general-style.styl' that containes global css variables and the 'color-scheme.styl' containing global colors (previous realization caused inclusion of all global css variables to each file that imported the general-file.styl after build).

### Version 1.24.1
### 2018-06-18 - Anastasiia Zhdaniuk
-  Fix issue with preventDefault on touchmove in Chrome v67.

### Version 1.24.0
### 2018-06-15 - Anastasiia Zhdaniuk
-  Fix issues for pre commit hook. Change path in configuration file.

### Version 1.23.1
### 2018-06-06 - Andrii Romanyshyn
-  Add functionality of excluded modules for Iplanner build.

### Version 1.23.0
### 2018-05-23 Oleksandr Melnyk
-  Move preventing default actions on touchmove from head.html to separate module.

### Version 1.22.0
### 2018-05-30 - Dmytro Kravchuk
-  Add 'general-style', delete 'color-scheme'. Rewrite main theme structure to css variables.

### Version 1.21.1
### 2018-05-22 - Anastasiia Zhdaniuk
-  Add functionality for build key message by cluster.

### Version 1.21.0
### 2018-05-22 - Anton Havryliaka
-  Add prevention of commit if linting errors are present in files processed to commit.

### Version 1.20.17
### 2018-05-10 - Maksym Mytiuk
-  Fix eslint autofix, added rule so that the debugger is not deleted.

### Version 1.20.16
### 2018-04-17 - Oleksandr Tkachov
-  Fix stylint warnings in appstyles.styl

### Version 1.20.15
### 2018-04-17 - Anastasiia Zhdaniuk
-  Add functionality for set 'state-default' class for popups

### Version 1.20.14
### 2018-04-13 - Anastasiia Zhdaniuk
-  Add ah-cleanup in presentation.json and index.pug

### Version 1.20.13
### 2018-03-21 - Nina Koval
-  update `ag-auto-menu.css` and `ag-video.css` according to csslint

### Version 1.20.12
### 2018-03-26 - Sikhnevych Kostiantyn
- Add functionality adding custom class for slideshow in `ag-viewer`.

### Version 1.20.11
### 2018-03-27 - Serhii Zviahelskyi
- hotfix in the presentation.json (the semicolon was deleted)

### Version 1.20.10
### 2018-03-27 - Serhii Zviahelskyi
- updated module 'ah-correct-events' to v1.1.0
- connected module 'ah-slide-transition' as default

### Version 1.20.9
### 2018-03-26 - Oleksandr Melnyk
- add task rm-prod-build-cleaner for additional deleting .css and .js in slides folders, add corresponding function to BuildCleaner

### Version 1.20.8
### 2018-03-23 - Maksym Mytiuk
- add eslint autofix for modules and slides

### Version 1.20.7
### 2018-03-23 - Serhii Zviahelskyi
- Added module `ah-slide-transition`.

### Version 1.20.6
### 2018-03-23 - Andrii Romanyshyn
- fixed ag-overlay wrong behaviour on exit state when unlock presentation.

### Version 1.20.5
### 2018-03-21 - Serhii Synianskyi
- update touchy.js to v1.4.2

### Version 1.20.4
### 2018-03-21 - Nina Koval
-  fix path to images in modules scripts `PreBuildBuilder.coffee`

### Version 1.20.3
### 2018-03-20 - Anton Havryliaka
- fix `_package.json` template (add `gulp-insert` dependency)

### Version 1.20.1
### 2018-03-20 - Andrii Dryzhuk
- add autofix flag for css lint

### Version 1.20.0
### 2018-03-19 - Anton Havryliaka
- Add optional inject of `strings.json` data in slide instance
- New slides will have commented `@slideStrings` property after generation
- Usage: uncomment `@slideStrings = '%=slideStrings%'` line in slide constructor (or add it to existing slide), after build it will inject `strings.json` content in scripts, so you can call them i.e.:
  `app.view.get('SlideName').slideStrings` - returns object with slide strings

### Version 1.19.7
### 2018-03-19 - Maksym Mytiuk
- Add .eslintignore to ignore js files and folders
- Removed esLintIgnore from package.json

### Version 1.19.6
### 2018-03-19 - Serhii Zviahelskyi
- Fixed the problem of generating the CSV file (issue if  slideshow has many slides).

### Version 1.19.5
### 2018-03-16 - Andrii Romanyshyn, Roman Kovalenko
- Fix rm-check-references for veeva build
- Update index.pug. Fix missed classes in veeva build

### Version 1.19.4
### 2018-03-15 - Roman Savitskyi
- Update config for stylus

### Version 1.19.2, 1.19.3
### 2018-03-15 - Roman Savitskyi
- Update rainmaker config. Fix problem with jquery version and clean-up module validations

### Version 1.19.1
### 2018-03-15 - Andrii Romanyshyn
- Fix rm-validate. Update package.json

### Version 1.19.0
### 2018-03-14 - Andrii Romanyshyn
- Add gulp rm-validate for checking duplicates, necessary modules and references validation: `gulp rm-validate`
- Add rm-check-references and config for references: `gulp rm-check-references`

### Version 1.18.0
### 2018-03-14 - Viktor Shevchuk
- fix styles linter

### Version 1.17.0
### 2018-03-12 - Anton Havryliaka
- add slush command `cluster-delete` to delete storyboards with given `country` value, i.e.:

`slush agnitio-rainmaker:cluster-delete somecountry` : deletes all storyboards with `"country": "somecountry"` value. Can use multiple values:

`slush agnitio-rainmaker:cluster-delete somecountry anothercountry` etc.

It also cleans up all corresponding slideshows and slides which belong to this storyboard

### Version 1.16.1
### 2018-03-08 - Serhii Synianskyi
- add property for ignoring checking of files by linter (ignore cdm.sketcher.min.js from ap-notepad)

### Version 1.16.0
### 2018-03-06 - Anastasiia Zhdaniuk
- Create functionality for generate config for source files in veeva wide VeevaSourceConfigGenerator.coffee and add this configure in config file into PreBuildBuilder.coffee

### 2018-03-06 - Anastasiya Shapovalova
- Add ah-cleanup module

### Version 1.15.0
### 2018-03-06 - Oleksandr Tkachov
- Add custom flag "sa" for --vw option for veeva wide bundled shared build into PreBuildBuilder.coffee
### 2018-03-05 - Kostiantyn Sikhnevych
- update slush slides-batch-delete task (it also deletes slides from test folder)

### 2018-03-05 - Viktor Shevchuk
- Add stylint for *.styl
- Add stylelint for *.css

### 2018-03-01 - Kostiantyn Sikhnevych
- Add possibility to set custom reaction property for Veeva in config file

### 2018-03-01 - Maksym Mytiuk
- fix styles in ag-video.css

### Version 1.14.0
### 2018-02-16 - Roman Savitskyi
- Add coffeelint
- Add eslint with airbnb lint rules

### Version 1.13.0
### 2018-02-14 - Serhii Zviahelskyi
- Update accelerator to version 1.4.7

### v1.12.4
##### 2018-01-23 - Roman Savitskyi
- Fix config variable for Veeva
- Fix highligh point in global css file

### v1.12.3
##### 2018-01-05 - Roman Savitskyi
- Add npm command for csv generation
- Update and add shared command

### v1.12.2
##### 2018-01-05 - Roman Savitskyi
- (Update) Default animation for overlay. Fix jumping content.

### v1.12.1
##### 2018-01-05 - Roman Savitskyi
- (Update) Add disable actions via config. Set default to "Swipe"

### v1.12.0
##### 2017-12-20 - Anton Havryliaka
- Add regexp `%--storyboard--%` in `index.pug` template.

This feature allows to add class which is equal to current collection id on `#presentation` DOM element in Veeva Wide build (with latest `anthill-cli` package). It gives opportunity to handle styles and other features based on current collection more smoothly.

### v1.11.1
##### 2017-12-20 - Anastasiia Zhdaniuk
 feature:
Update styles-custom.css. Reset margin for figure.

### v1.11.0
##### 2017-12-20 - Anton Havryliaka
- Add `full-veeva.png` thumbnail for slide creation assets

### v1.10.3
##### 2017-12-04 - Roman Kovalenko, Roman Savitskyi
 feature:
Update veeva csv generator. Fix naming presentation for Veeva generator.

### v1.10.2
##### 2017-11-24 - Roman Kalensky, Roman Savitskyi
 feature:
Update bottom.html (slides scripts injection, open pdf subscription)

### v1.10.1
##### 2017-11-09 - Roman Savitskyi
 remove:
unused modules

### v1.10.0
##### 2017-11-02 Anastasiya Zdanyuk (migrator), Roman Savitskyi
- Fix veeva wide including
- Fix ag-viewer for Veeva Wide build
- Update ignore files for tmp build
- Add optimizer and correct events modules as default modules to build. Without those modules presentations will be crashed


### v1.9.1
##### 2017-10-19 Anastasiya Shapovalova
- Fix prefix for Veeva Wide build

### Roman Savitskyi, Anastasiya Zdanyuk
- Add command for update engine to latest version.

### Version 1.9.0 2017-10-19
### Roman Savitskyi, Anastasiya Zdanyuk
- Add command for update engine to latest version.

  #####After run 'slush agnitio-rainmaker:update' it does next points:
   - Change all .jade files to .pug
   - Rename folder jade_fragments (if exist) to pug_fragments
   - Delete all .jade files
   - Setup default command for install engine 'slush agnitio-rainmaker'
   - Replace files after finished 'slush agnitio-rainmaker' (structure-strings.json, veeva/configs.json, global-strings.json, references.json, references-strings.json, media.json, common.styl, color-scheme.styl, index.pug, config.json)
   - Update .jade to .pug in presentation.json
   - Return correct version in package.json after finished 'slush agnitio-rainmaker'
   - Return correct startPath in rainmaker/config.json after finished 'slush agnitio-rainmaker'
   
### Version 1.8.1 2017-10-12
### Roman Savitskyi, Mykhailo Sosin
- Add Veeva Deep detection

### Version 1.8.0 2017-10-11
### Nina Koval, Viktor Shevchuk
- Update accelerator to latest version
- Update Veeva Wide CSV generator

### Version 1.7.2 2017-09-28
### Anastasiya Zdanyuk 
- Update engine to Node 8.5.0. Support all packages

### Version 1.7.1 2017-09-26
### Mykola Fant 
- Add validator for common modules

### Version 1.7.0 2017-09-07
### Roman Savitskyi. 
- Create Veeva Wide builder with shared keymessage.

  ````npm run vw-shared```` command for build presentation with shared keymessage.
  


### Version 1.6.4 2017-09-07
### Roman Savitskyi. 
- Delete unusage files for production build. Clear structure.
- Modify events for viewer. Skip ag.on and migrate to app.listen for skip ag executing in Veeva Wide version.

### Version 1.6.3 2017-08-29
### Roman Savitskyi. Nina Samsonova
- Fix problem with inline slideshow.

### Version 1.6.2 2017-08-07
### Roman Savitskyi. Anastasiya Zhdanyuk
- Update overlay for working with history module. Add possibility to setup type and delay as props.

### Version 1.6.1 2017-08-03
### Roman Savitskyi.
- Fix fonts including. Update generate base64 fonts

### Version 1.6.0 2017-08-02
### Roman Savitskyi. Sosin as migrator
- Update font generation. Engine generate fonts without any styl files.

### Version 1.5.0 2017-06-26
### Roman Savitskyi, Max Dyachenko
- Add duplicate image finder.


### Version 1.4.1 2017-06-26
### Roman Savitskyi, Nina Koval
- Add Veeva Wide mode. Support Veeva Wide templates

### Version 1.4.0 2017-06-23
### Roman Savitskyi
- update Veeva CSV generator. Create strcutures and sldies according to collections

### Version 1.3.2 2017-06-19
### Roman Savitskyi
- clean-css updated to latest version. Fix issue with incorrect clear border-image-splice

### Version 1.3.1 2017-05-25
### Roman Savitskyi
- gulp vv-single is deprecated

### Version 1.3.0 2017-05-22
### Roman Savitskyi
- Add renamer from jade_fragments to pug_fragments folder
- Add Veeva csv generator for pdfs for Veeva Wide build
- Fix zip problem in Veeva deep build
- Rename Jade to Pug

### Version 1.2.3 2017-05-22
### Roman Savitskyi
- Update px to rem. Add production modes, excluded tags.

### Version 1.2.1 2017-05-22
### Serhii Zviahelskyi
- Add gulp-stylus, postcss-pxtorem, gulp-postcss

### Version 1.2.0 2017-05-22
### Roman Savitskyi
- Update ag-overlay.
- Fix problem with onEnter/onExit tracking.
- Fix problem with double opening one popup.
- Add functionality for open popup without SLIDE content. For open modules functionality in popup as sitemap.

### Version 0.1.3 2017-05-22
### Roman Savitskyi
- Add Veeva presentation config.

### Version 0.1.2 2017-05-03
### Roman Savitskyi
- Update presentation.json to correct structure

### Version 0.1.1 2017-05-03
- Add npm-debug-.log to git ignore

### Version 0.0.0 2017-03-23
- Initial release
