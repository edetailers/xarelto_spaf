# Devkit Documentation
####1.0 - Tor Kristensen [tkr@anthillagency.com](mailto:tkr@anthillagency.com)

___

### Application Info :

- ## ID/NAME = "c2_2019_spaf_main_story_collection / AA.XARELTO.SPAF_MAIN_STORY_CYCLE_2_2019.1933-EN"
- ## ID/NAME = "aa_xarelto_spaf_main_story_c2_2019_pdfs / AA.XARELTO.SPAF_MAIN_STORY_PDFS_CYCLE_2_2019.1932-EN"
- ## CLM system = "Veeva Wide"

- ## ID/NAME = "109565 / AA.XARELTO.SPAF_MAIN_STORY_CYCLE_2_2019.1933-EN"
- ## CLM system = "Iplanner"

- ## ID/NAME = "21625 / AA.XARELTO.SPAF_MAIN_STORY_CYCLE_2_2019.1933-EN"

- ## CLM system = "Rainmaker"

** Iplanner, Pitcer, Veeva, Cegedim **
# Installation
Below are recommended apps you should have on your PATH.

1. **NodeJS** and  **NPM**
    * install from [website](http://nodejs.org/)

2. **Gulp **
    * install via : ```npm i gulp -g ```

3. **Slush **
    * install via : ```npm i slush -g ```
___

# Gulp Commands


Below are a series of gulp commands you can use to build your project for the Agnitio and Veeva CLM platforms.

___
___


### Agnito Platform


* **Just Build it**

    ``` gulp rm ```

    Builds your project.

* **Live-reload Development**

    ``` gulp rm-watch ```

    * Builds your project for agnitio.

    * Creates a localhost webserver.

    * Creates a Livereload server.

    * Opens the detailer in you default webbrowser.
    * Watches your source code for changes.

    * Whenever you save a file or alter an image, your browser will automatically load the changes.

    **Tip:** Use the [Chrome Livereload plugin](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei)

* **Clean build directory**

    ``` gulp rm-clean ```

* **Validate references**

    ``` gulp rm-check-references ```

    * Check reference keys on slides
    * For this functionality required ``` app/references-config.json ```

    Example of references-config.json:
    ```json
        {
            "references": {
                "file": "media.json",
                "dataAttr": "data-reference-id",
                "separator": ",",
                "groupSeparator": "-"
            }
        }
    ```

 * **Validate presentation**

     ``` gulp rm-validate ```

     Run gulp rm-check-modules && rm-find-duplicate && rm-check-references

    Deletes all files + folders from the ```app/build``` directory
* **Clean build directory**

    ``` npm run dev ```

    Run gulp rm-clean && gulp rm && gulp rm-watch


### Veeva Platform

* **Set ```key_message_names``` in ```app/platform/veeva/config.json``` default key use for create default key message name. If you need add name for some cluster you need add new key and value ```"cluster_name": "test_cluster_name"```**

* **Build Agnitio presentation**

     ``` gulp rm ```
* **Build Veeva deep presentation**

    ``` gulp vv-single ```

* **Build Veeva deep presentation (new format of package)**

    ``` gulp vv-single-new ```

    If you want build media files as separate veeva presentation please use ``` gulp vv-single-new --wide-media ``` command but before run this command please do points 4 and 6 (see below).

* **For generate key message for cluster use ``gulp vv-single --cluster=cluster_name``**


* **Build veeva wide presentation**

**note: For correct working navigation between key messages in veeva wide mode please add module `ah-get-veeva-wide-order` from `anthill-components` and path to it in index.pug `#veevaWideOrder(data-module='ah-get-veeva-wide-order')`.
	It helps to fix issue with double sending clickstream monitoring from the first and the last key messages in irep. That bug appears when user swipes left on the last key message and swipes right on the first keymessage. **

1. Generate csv files:
Please add correct structure of config.json. General fields:

`product_name` - name of product for presentations and key messages in vault.
`country` - name of country for presentations and key messages in vault.
`language` - name of language for presentations and key messages in vault.
`veeva_id` - Veeva vault id
`pdfPresentation` - object with fields for presentation with pdfs.
`shared_key_message` - object with fields for shared key message.
`presentations` - array of presentations objects - each object is
matches to collection in presentation.json.

Example of config.json:
```json
{
    "veeva": {
        "product_name": "Testing - Tier 1",
        "country": "Global",
        "veeva_id": 7930,
        "pdfPresentation":{
            "presentation_name": "default_structure_from_generator",
            "presentation_id": "default_structure_from_generator",
            "presentation_prefix": "default_structure_from_generator_",
            "distributionGroup": "",
            "businessUnit": "",
            "indication": "",
            "related_resource_id": "",
            "hidden": "YES",
            "document_id": "",
            "document_start_slide__id": ""
        },
        "shared_key_message": {
            "name": "testing Shared Resources",
            "id": "testing Shared Resources",
            "document_id": ""
        },
        "presentations": [
            {
                "collection_id": "home_collection",
                "presentation_name": "default_structure_from_generator",
                "presentation_id": "default_structure_from_generator",
                "presentation_prefix": "default_structure_from_generator",
                "distributionGroup": "",
                "businessUnit": "",
                "indication": "",
                "related_resource_id": "",
                "country": "core",
                "product": "default",
                "hidden": "NO",
                "document_id": "",
                "document_start_slide__id": "",
                "customReaction": "NONE"
            }
        ]
    }

}
```
######Fields for presentation object in `presentations` array:

`collection_id` - id of collection - can be get from presentation.json
`presentation_name` - name of presentation in veeva.
`presentation_id` - external id of presentation in veeva - should be unique.
`presentation_prefix` - prefix for key messages. It can be the same for all slides in one collection, if you provide this field. If you don't have this files in presentation object in config.json, as prefix will be use slideshow id from presentation.json for each slide. For example: home_collection_FrontPageSlide
`country` - country code for filtration of presentations.
`product` - product code for filtration of presentations.
`hidden` - hide presentation or not.
`related_resource_id` - document id of shared key message.
`document_id` - document id of presentation in veeva vault.
`document_start_slide__id` - document id of first key message in vault presentation.
'customReaction' - to disable the reaction buttons in veeva add "NONE". To display reaction buttons don't add anything. If in veeva was uploaded slides with field "custom action": "NONE" you can delete it only in veeva settings.
`distributionGroup`, `businessUnit`, `indication` - fields ONLY for for Content Factory (Bayer) environment (with using prefix `--cf`)

######Fields for `pdfPresentation` object is described above in presentation object fields:

######Fields for `shared_key_message` object:
`name` - name of shared key message in veeva vault.
`id` - id of shared key message.
`document_id` - document id in veeva vault.

######Kindly note that the fields `document_id`, `document_start_slide__id`, `related resource_id` should be empty on first csv generation (as in config.json example). In this case veeva vault will create presentation and all it key messages. After success creation, we should fill this fields according to ids of created resources, regenerate csv files. After this we can update presentation and all it resources in case of any changes.

2. Run command:
`gulp vw-csv` - generate `multichannelDataCreate.csv` file with all presentations
prefixes:
`--product spaf` - generate all presentations with field "product": "spaf"
`--country br` - generate all presentations with field "country": "br"
`--shared` - generate presentations with shared key message
`--cf` - generate presentations with unique fields ONLY for Content Factory (Bayer) env
`--analytics` - generate data for fields `Name` and `Description` from `analytics.json`
Also you can find this command in npm scripts - `vw-csv`.
`gulp vw-pdf-csv` - generate `multichannelPdfCreate.csv` file for presentation with pdfs. This command was added into npm scripts - `vw-pdf-csv`
prefixes:
`--cf` - generate presentations with unique fields for for Content Factory (Bayer) env

3. Result: In `multichannelDataCreate.csv` and `multichannelPdfCreate.csv` files you will have data for uploading key messages with using multichannel loader of veeva vault.

4. After generate `multichannelPdfCreate.csv` file you should upload pdfs in veeva vault. For correct goTo on pdfs in veeva wide presentation you need generate `app/pdfs-config.json`. This files includes name for pdfs from our presentation and correct media file for keymessage from veeva. For correct generate this file you should do next points: 1) After upload pdfs in veeva vault you should check `veeva_id` field and add `document_start_slide__id` (document id for first key message in vault presentation) for `pdfPresentation` in `app/config.json`. 2) After fields ware checked please run `vw-pdf-config`. This command generate file `app/pdfs-config.json`. 3) After file `app/pdfs-config.json` was generated got to next point(5.) and rebuild presentation for upload in veeva wide.

5. For build key messages install https://www.npmjs.com/package/anthill-cli or update to latest version.
and use ah a2v command for build
custom prefixes:
`ah a2v -t` - generate key messages with thumbnails from genkins (they should be placed in app/slideId/thumbnails/full-veeva.png 1024x768).
`ah a2v -stlc` - generate key message with additional optimization and thumbnails
`ah a2v --prefix prefixName_` - generate key messages with custom prefix, in another way should be used slideshows id from presentation.json
`ah a2v --delay 5` - delay time for screenshots if you will generate thumbnails with screenshots generator.
In case uploading presentation, in folder packages you will have build packages for uploading using multichannel loader.
In case uploading shared_key_message, in folder packages you will have package "shared.zip".
In case uploading pdfs presentation, you can use pdf files for uploading using multichannel loader.

6. If you want to upload videos as separate key messages into the veeva clm, please put them to the `shared/videos` folder. Then you should generate `videos-config.json` - with the same structure as for `pdfs-config.json`. Please use for it command `gulp vw-video-config`. But previously please write down correct info about video presentation to the `config.json`. Firstly, you will upload videos without ids of presentation and first key message and then please add them. For generation `multichannelVideoCreate.csv` please use command: `gulp vw-video-csv`.
 ----------------------------------
  7. How to generate `analytics.json`:
     - clone `https://bitbucket.org/anthillagency/xarelto-analytics-cli`
     - in presentation run `xana export`
     - paste new data to `Xarelto Clickstream Analytics - Content Mapping.xlsx`
     - run `xana excel2json "Xarelto Clickstream Analytics - Content Mapping.xlsx`
     - received `analytics.json` paste to `app/` folder

     ### Important notes:
        - To build correct analytic for 2019 Xarelto you need to update `xarelto-analytics-cli`. In file xarelto-analytics-cli\lib\utils\create-export-data.coffee you need to find hardcoded variable `indx1=parts.indexOf '2018'` and change `2018` to `2019` http://prntscr.com/lfe8vc.
        - If some pdfs don't work, than you need to check `Description` column in excel file and in analytics-data-from-detail-structure.json. If we have some unallowed symbols like apostrophe and ect or we have opener bracket without closer bracket it will cause error.
            - Always check ref descriptions for Fulcher:2015ta, LopesR.D:2012uk, Anand:2017fb
   ----------------------------------

   Important!!! If you use different ids for presentation in veeva, then please fill down them in field `presentation_id` and regenerate file `presentations-config.json` using command `gulp vw-presentations-config`
### Cluster Build

 #####For building presentation by cluster:

 1. Set 'cluster' value for collections, popups, slideshows.
 ```json
 {
 "storyboards": {
        "start_collection": {
            "name": "Start presentation",
            "id": "start_collection",
            "type": "collection",
            "cluster": "myCluster",
            "start": true,
            "content": [
                "default_structure_from_generator"
            ]
        }
    }
 }
 ```
  Note: for each start cluster collection key "start" is required!

 2. Run command (Filtered by cluster structure and slides will be in tmp folder):
    `gulp rm-clean-tmp && gulp rm-cluster-build --cluster=myCluster`

 3. For running presentation by cluster in develop mode please run command:
    `gulp rm-clean-tmp && gulp rm-cluster-build --cluster=myCluster && gulp rm-clean && gulp rm --env development --cluster=myCluster && gulp rm-watch`

### Cegedim Platform

##### If you want to upload presentation into cegedim by cluster, you should do next actions:
1. Fill cegedim config.json (app/platforms/cegedim/config.json).
Note: key should be equival to value "cluster" in presentation.json and value of 'cluster' from gulp command. In result in cegedim we will have presentation with name 'test-presentation-myCluster-test'
```json
 {
 "sequencePrefix": "test-presentation",
 	"sequenceSuffix": {
 		"myCluster": "myCluster-test"
 	}
 }
 ```
2. Add icon for cluster into shared/images/icons/. Name of icon should be equival to cluster value, size should be 200x150. For example 'myCluster.jpg'
3. Build presentation by cluster (see 'Cluster Build' description) .
4. Run ```gulp cg-single```

* **Set icon image in shared/images/icons/200x150.jpg with size 200px on 150px.**

* **Build Agnitio presentation**

     ``` npm run dev or npm run prod ```

  If you want to delete modules for agnitio build, you need:
    1. Add attributes in presentation.json for target modules:
        `````"ignoreClm": ["veeva_deep", "agnitio", "cegedim"]`````
    3. In `````ignoreClm````` atribute add to array target clm. List of clms: `````"veeva_deep", "iplanner", "veeva_wide", "cegedim"`````
    2. Use flag `````--vd````` - for veeva deep build, `````--ag````` - for agnitio build and `````--cg````` - for cegedim build in prod script for ````gulp rm-prod-build-cleaner --vd````.
       Or you can use scripts prod-vd, prod-ip, prod-cg to build your presentation without redundant modules.

* **Build Veeva Wide presentation with shared keymessage**

    ```npm run vw-shared```

    If you want to delete files or libs for Veeva Wide, you can add it to config exclude array.

   If you want to delete modules for Veeva, you can add attributes in presentation.json for current module
    `````   "ignoreVeeva": true`````
---------------------------------
* **Build Cegedim deep presentation**

    ``` gulp cg-single ```
---------------------------------

# Slush Commands

Here is a list of slush commands you can use to create DevKit projects, and to add new Slides.

 * ```slush agnitio-rainmaker```

    Will create a new Devkit project in a directory

 * ```slush agnitio-rainmaker:slide YourSlideName```

    Create a new Slide ( Yourclass inherits from Slide ) and add it to your project
 * ```slush agnitio-rainmaker:slidebasic YourSlideName```

    Create a new Slide instance, and attach event listeners.

    Allowed, but not recommended.

 * ```slush agnitio-rainmaker:example-assets```

    Copies example images, fonts, etc into your shared-assets folder for use.

 * ```slush agnitio-rainmaker:framework```

    Copy the Agnitio framework assets to your project.
___

# Adding fonts to project

 Font files are converted into a series of @font-face declarations containing each font encoded in base64.

 **Input font files**

 Font weight should be one of 100, 200, 300, 400, 500, 600, 700, 800, 900. You may also provide a commonly used weight name (except normal) that maps to one of these values.

 Font style should be one of normal, italic or oblique.

@font-face property values are determined by the input file names, which must obey the following naming scheme with dash-separated attributes:

```<family>[-<weight>][-<style>].<extension>```

For example, the following are valid names:

* ```Lato.woff```
* ```Lato-italic.woff```
* ```Lato-bold.woff```
* ```Lato-700.woff```
* ```Lato-thin-italic.woff```
* ```Lato-100-italic.woff```
___

# LFS

### For working with this repo please use git lfs:
    https://www.atlassian.com/git/tutorials/git-lfs#installing-git-lfs

### Please ensure in that all branches include .gitattributes file with filters for lfs
    https://bitbucket.org/anthillagency/aa-xarelto-cycle-2-2018/src/1721ec5fe4b0da57c8e252c6d6ad73d8acc5a610/.gitattributes
    link above like example

Assets files with such extensions are pushed to lfs server:
* ```.pdf```
* ```.mp4```
* ```.avi```
* ```.mp3```
* ```.png```
* ```.jpg```

### use next commands:
    > git lfs track - activate filters
    > git lfs fetch --all - get all files from lfs
    > git lfs ls-files
    > git lfs push origin <branch>

### in order to see all files that are saved on lfs server:
    > git lfs ls-files

### in order to move file to LFS server:
    > git rm --cached <path_to_png>/*.png
    > git add <path_to_png>/*.png
    > git commit -m "Convert <path_to_png>/*.png files to LFS"

### for more info please check documentation and command git lfs help
